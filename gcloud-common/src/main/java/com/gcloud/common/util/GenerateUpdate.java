package com.gcloud.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class GenerateUpdate {

    public static void main(String[] args) {

        //换成对应实体�?
        Class clazz = GenerateUpdate.class;

        Field[] fields = clazz.getDeclaredFields();

        for(Field field : fields){
            if(isExclude(field)){
                continue;
            }

            System.out.println(String.format("public static final String %s = \"%s\";", underlineUpper(field.getName()), field.getName()));
        }

        for(Field field : fields){

            if(isExclude(field)){
                continue;
            }

            String fileName = field.getName();
            String upperName = StringUtils.toUpperCaseFirstOne(fileName);
            String typeName = field.getType().getSimpleName();
            System.out.println(String.format("public String update%s (%s %s){", upperName, typeName, fileName));
            System.out.println(String.format("this.set%s(%s);", upperName, fileName));
            System.out.println(String.format("return %s;", underlineUpper(field.getName())));
            System.out.println(String.format("}"));
        }


    }

    public static String underlineUpper(String str){

        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < str.length(); i++){

            char c = str.charAt(i);
            if(Character.isUpperCase(c)){
                sb.append("_");
            }
            sb.append(c);
        }

        return sb.toString().toUpperCase();

    }

    public static boolean isExclude(Field field){
        if(Modifier.isStatic(field.getModifiers())){
            return true;
        }

        return false;
    }

}