package com.gcloud.core.currentUser.policy.service;

import org.apache.commons.lang.StringUtils;

import com.gcloud.core.currentUser.enums.RoleType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.common.ResourceOwner;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Slf4j
public abstract class ResourceIsolationCheckImpl implements IResourceIsolationCheck {

	@Override
	public void check(String resourceId, CurrentUser currentUser) throws GCloudException {
		ResourceOwner resourceOwner = getResourceTenantId(resourceId);
		if(resourceOwner == null) {
			log.error("找不到操作的资源resourceId:" + resourceId + ",currentUser.defaultTenant:" + currentUser.getDefaultTenant() + ",currentUser.id:" + currentUser.getId());
			throw new GCloudException("找不到操作的资源");
		}
		if(!currentUser.getRole().equals(RoleType.SUPER_ADMIN.getRoleId())) {
			IsolationTypeChecks.get(getIsolationType().getValue()).check(resourceOwner, currentUser);
			
		} else {//超级管理�?
			
			//租户隔离
			if(StringUtils.isNotBlank(currentUser.getDefaultTenant())) {
				if(!currentUser.getDefaultTenant().equals(resourceOwner.getTenantId())) {
					throw new GCloudException("当前用户没有权限操作该资�?");
				}
			}
		}
	}
	
	public abstract ResourceOwner getResourceTenantId(String resourceId);
}