package com.gcloud.core.async;

import com.gcloud.core.service.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Slf4j
@ConditionalOnExpression("${gcloud.controller.asyncThreadController.checkThread.enable:false} == true")
public class AsyncCheckTimer {

    //比超时时间延迟的时间，默认延�?5分钟
    @Value("${gcloud.controller.asyncThreadController.checkThread.interruptedDelay:300000}")
    private Long interruptedDelay;

    @Scheduled(fixedDelayString = "${gcloud.controller.asyncThreadController.checkThread.scheduleRate:300000}")
    public void check(){

        log.debug("AsyncCheckTimer begin");

        Collection<GcloudThreadPool> pools = SpringUtil.getBeans(GcloudThreadPool.class);
        if(pools == null || pools.isEmpty()){
            return;
        }

        for(GcloudThreadPool pool : pools){
            checkPool(pool);
        }

        log.debug("AsyncCheckTimer end");

    }

    public void checkPool(GcloudThreadPool pool){

        log.debug(String.format("check pool %s begin", pool.getClass().getName()));

        if(pool == null){
            log.debug("AsyncCheckTimer end, pool is null");
            return;
        }

        log.debug("===pool active count===" + ((ThreadPoolExecutor)pool.getExecutor()).getActiveCount());

        Map<String, AsyncInfo> infoMap = new HashMap<>();
        infoMap.putAll(pool.getAsyncTasks());
        if(infoMap.size() == 0){
            log.debug("AsyncCheckTimer end, there is no thread in info map");
            return;
        }

        for(Map.Entry<String, AsyncInfo> asyncInfo : infoMap.entrySet()){
            try{
                AsyncInfo info = asyncInfo.getValue();
                if(info == null){
                    pool.removeAsyncTask(asyncInfo.getKey());
                }else{
                    Future<?> future = info.getFuture();
                    if(future.isDone()){
                        log.debug("async is done, uuid=" + asyncInfo.getKey());
                        pool.removeAsyncTask(asyncInfo.getKey());
                    }else{

                        //延迟再中�?
                        boolean isTimeout = System.currentTimeMillis() - info.getStartTime() > info.getTimeout() + interruptedDelay;
                        if(isTimeout){
                            log.debug("async is timeout, uuid=" + asyncInfo.getKey());
                            future.cancel(true);
                            pool.removeAsyncTask(asyncInfo.getKey());
                            log.debug("async is canceled, uuid=" + asyncInfo.getKey());
                        }

                    }
                }
            }catch (Exception ex){
                log.error("async check error", ex);
            }
        }

        log.debug(String.format("check pool %s end", pool.getClass().getName()));

    }

}