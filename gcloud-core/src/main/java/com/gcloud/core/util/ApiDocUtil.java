package com.gcloud.core.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFStyle;
import org.apache.poi.xwpf.usermodel.XWPFStyles;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDecimalNumber;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTOnOff;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTString;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTStyle;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STStyleType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;

import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.api.model.ApiBody;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDocUtil {
	private final static ApiDocUtil INSTANCE = new ApiDocUtil();
	public static ApiDocUtil getInstance() {
        return INSTANCE;
    }
	private Map<Module,Map<SubModule,Map<String,Class>>> moduleHandlers=new HashMap<>();
	private Map<ApiVersion, Map<Module,Map<SubModule,Map<String,Class>>>> versionHandlers = new HashMap<>();
	
	private String mdStr;
	
	public String getDoc(){
		if(mdStr==null)
			try {
				init();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return mdStr;
	}
	
	public void createMsDoc(String filePath, ApiVersion[] versions) {
		try{
			msInit(versions);
			createDocWithVersion(filePath, getDocDataTypeWithVersion());
		} catch(ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public void init() throws ClassNotFoundException{
		Map<String,Object>  map=SpringUtil.getApplicationContext().getBeansWithAnnotation(ApiHandler.class);
		moduleHandlers=new HashMap<>();
		for(Object o:map.values()){
			String className= o.getClass().getName().substring(0, o.getClass().getName().indexOf("$$"));
			Class<?> clazz = null;
			clazz = Class.forName(className);
			put(clazz);
			/*Type types = clazz.getGenericSuperclass();
	        Type[] genericType = ((ParameterizedType) types).getActualTypeArguments();
	        if(genericType.length>0){
	        	Class reqClazz= Class.forName(genericType[0].getTypeName());
	        	Field[] reqFields= ReflectionUtils.getDeclaredField(reqClazz);
	        	for(Field field:reqFields){
	        		field.getName();
	        	}
	        	Class respClazz= Class.forName(genericType[1].getTypeName());
	        }
	        */
		}
		mdStr=genMdStr();
		
	}
	
	private void msInit(ApiVersion[] versions) throws ClassNotFoundException {
		Map<String,Object>  map= SpringUtil.getApplicationContext().getBeansWithAnnotation(ApiHandler.class);
		versionHandlers = new HashMap<>();
		for(Object o:map.values()){
			String className= o.getClass().getName().substring(0, o.getClass().getName().indexOf("$$"));
			Class<?> clazz = null;
			clazz = Class.forName(className);
			put(clazz, versions);
		}
	}
	
	public void put(Class<?> clazz){
		ApiHandler  apiHandler= clazz.getAnnotation(ApiHandler.class);
		Module module=apiHandler.module();
		SubModule subModule=apiHandler.subModule();
		String action=apiHandler.action();
		Map<SubModule,Map<String,Class>> subMap= moduleHandlers.get(module);
		if(subMap==null){
			subMap=new HashMap<>();
			Map<String,Class> actionMap=new HashMap<>();
			actionMap.put(action, clazz);
			subMap.put(subModule, actionMap);
			moduleHandlers.put(module, subMap);
		}else{
			Map<String,Class> actionMap=subMap.get(subModule);
			if(actionMap==null){
				actionMap=new HashMap<>();
				actionMap.put(action, clazz);
				subMap.put(subModule, actionMap);
			}else{
				actionMap.put(action, clazz);
			}
		}
			
	}
	
	private void put(Class<?> clazz, ApiVersion[] versions){
		ApiHandler  apiHandler= clazz.getAnnotation(ApiHandler.class);
		Module module=apiHandler.module();
		SubModule subModule=apiHandler.subModule();
		String action=apiHandler.action();
		ApiVersion[] apiVersions = apiHandler.versions();
		ApiVersion[] needVersions = getRepeat(versions, apiVersions);
		
		for (ApiVersion version : needVersions) {
			Map<Module,Map<SubModule,Map<String,Class>>> moduleMap = versionHandlers.get(version);
			if(null == moduleMap) {
				moduleMap = new HashMap<>();
				Map<SubModule,Map<String,Class>> subMap = new HashMap<>();
				Map<String,Class> actionMap = new HashMap<>();
				actionMap.put(action, clazz);
				subMap.put(subModule, actionMap);
				moduleMap.put(module, subMap);
				versionHandlers.put(version, moduleMap);
			} else {
				Map<SubModule,Map<String,Class>> subMap = moduleMap.get(module);
				if(null == subMap){
					subMap = new HashMap<>();
					Map<String,Class> actionMap = new HashMap<>();
					actionMap.put(action, clazz);
					subMap.put(subModule, actionMap);
					moduleMap.put(module, subMap);
					versionHandlers.put(version, moduleMap);
				}else{
					Map<String,Class> actionMap = subMap.get(subModule);
					if(null == actionMap){
						actionMap = new HashMap<>();
						actionMap.put(action, clazz);
						subMap.put(subModule, actionMap);
					}else{
						actionMap.put(action, clazz);
					}
				}
			}
		}
	}
	
	private String genMdStr() throws ClassNotFoundException{
		StringBuilder sb=new StringBuilder();
		sb.append("[[TOC]] <br/>");
		Iterator<Module> it=moduleHandlers.keySet().iterator();
		while(it.hasNext()){
			Module module=it.next();
			sb.append(MarkdownUtils.title(1, module.getCnName()));//主模�?
			Map<SubModule,Map<String,Class>> subMap=moduleHandlers.get(module);
			Iterator<SubModule> subIt=subMap.keySet().iterator();
			while(subIt.hasNext()){
				SubModule subModule=subIt.next();
				if(subModule!=SubModule.NONE){
					sb.append(MarkdownUtils.title(2, subModule.getCnName()));//子模�?
				}
				Map<String,Class> actionMap=subMap.get(subModule);
				Iterator<String> actionIt=actionMap.keySet().iterator();
				while(actionIt.hasNext()){
					String action=actionIt.next();
					Class<?> clazz=actionMap.get(action);
					ApiHandler apiHandler=clazz.getAnnotation(ApiHandler.class);
					if(StringUtils.isNotBlank(apiHandler.name())){
						sb.append(MarkdownUtils.title(3, apiHandler.name()));//功能�?
					}else {
						GcLog gclog=clazz.getAnnotation(GcLog.class);
						if(gclog!=null) {
							sb.append(MarkdownUtils.title(3, gclog.taskExpect()));//功能�?
						}
					}
					sb.append(MarkdownUtils.title(4, action));
					sb.append("URI:").append(module.name().toLowerCase()).append("/").append(action).append(".do\n");//url
					sb.append(MarkdownUtils.title(5, "请求参数:"));//请求参数
					Type types = clazz.getGenericSuperclass();
			        Type[] genericType = ((ParameterizedType) types).getActualTypeArguments();
			        if(genericType.length==2){
			        	Class reqClazz= Class.forName(genericType[0].getTypeName());
			        	Field[] reqFields= ReflectionUtils.getDeclaredField(reqClazz);
			        	sb.append(MarkdownUtils.tableTitle("参数","类型","必须?","例子","描述"));
			        	for(Field field:reqFields){
			        		ApiModel apiModel= field.getAnnotation(ApiModel.class);
			        		if(apiModel!=null){
			        			sb.append(MarkdownUtils.tableRow(field.getName(),apiModel.type(),apiModel.require()+"",apiModel.example(),apiModel.description()));
			        		}
			        	}
			        	sb.append(MarkdownUtils.title(5, "响应:"));//响应
			        	Class respClazz= Class.forName(genericType[1].getTypeName());
			        	Field[] respFields=ReflectionUtils.getDeclaredField(respClazz);
			        	List<com.gcloud.header.api.model.ApiModel> apiModels=addModel(respFields);
			        	sb.append(MarkdownUtils.tableTitle("参数","类型","例子","描述"));
			        	for(com.gcloud.header.api.model.ApiModel apiModel:apiModels){
			        		sb.append(MarkdownUtils.tableRow(apiModel.getName(),apiModel.getType(),apiModel.getExample(),apiModel.getDescription()));
			        	}
			        }
				}
			}
		}
		return sb.toString();
	}
	
	
	private List<List<Field>> getAllFields(List<Field> chain, Field[] fields, Class<?> gClass) {
		List<List<Field>> result = new ArrayList<List<Field>>();
		for (Field field : fields) {
			if(field.getAnnotation(ApiModel.class)==null)
				continue;
			Class<?> fieldClass = field.getType();
			if (fieldClass.isPrimitive() || fieldClass.getName().startsWith("java.math") || fieldClass.getName().startsWith("java.lang") || fieldClass.getName().startsWith("java.util.Date") || fieldClass.getName().startsWith("javax") || fieldClass.getName().startsWith("com.sun") || fieldClass.getName().startsWith("sun") || fieldClass.getName().startsWith("boolean") || fieldClass.getName().startsWith("double") || fieldClass.getName().startsWith("int")) {
				List<Field> endChain = new ArrayList<Field>(chain);
				endChain.add(field);
				result.add(endChain);
				continue;
			} else {
				if (!fieldClass.equals(PageResult.class)) {
					List<Field> endChain = new ArrayList<Field>(chain);
					endChain.add(field);
					result.add(endChain);
				}

				Type fc = field.getGenericType();
				if (field.getGenericType() instanceof ParameterizedType) { // 判断是否有泛型类�?

					ParameterizedType pt = (ParameterizedType) fc;

					Class<?> genericClazz = null;
					if ("sun.reflect.generics.reflectiveObjects.TypeVariableImpl".equals(pt.getActualTypeArguments()[0].getClass().getName())) {
						genericClazz = gClass;
					} else {
						genericClazz = (Class<?>) pt.getActualTypeArguments()[0]; // �?4�?
					}

					if (genericClazz.getName().startsWith("java.lang") // 设置list的终止类�?
							|| genericClazz.getName().startsWith("java.util.Date") || genericClazz.getName().startsWith("javax") || genericClazz.getName().startsWith("com.sun") || genericClazz.getName().startsWith("sun") || genericClazz.getName().startsWith("boolean") || genericClazz.getName().startsWith("double") || genericClazz.getName().startsWith("int")) {
						continue;
					}
					// System.out.println(genericClazz);
					// 得到泛型里的class类型对象�?
					List<Field> thisChain = new ArrayList<Field>(chain);

					if (!fieldClass.equals(PageResult.class)) {
						thisChain.add(field); // !!
					}

					if (fieldClass.isAssignableFrom(PageResult.class)) {
						result.addAll(getAllFields(new ArrayList<Field>(thisChain), ReflectionUtils.getDeclaredField(fieldClass), genericClazz));
					} else {
						result.addAll(getAllFields(new ArrayList<Field>(thisChain), ReflectionUtils.getDeclaredField(genericClazz), null));
					}
				} else {
					List<Field> thisChain = new ArrayList<Field>(chain);
					thisChain.add(field);
					result.addAll(getAllFields(new ArrayList<Field>(thisChain), ReflectionUtils.getDeclaredField(fieldClass), null));
				}

			}
		}
		return result;
	}
	
	private  List<com.gcloud.header.api.model.ApiModel> addModel(Field[] responseFields) {
		List<com.gcloud.header.api.model.ApiModel> amList = new ArrayList<com.gcloud.header.api.model.ApiModel>();
		List<List<Field>> responseFieldList = getAllFields(new ArrayList<Field>(), responseFields, null);
		if (responseFieldList != null) {
			for (List<Field> responseField : responseFieldList) {
				if (responseField != null && responseField.size() > 0) {
					int size = responseField.size();
					String name = "";
					for (int i = 0; size > i; i++) {
						name += responseField.get(i).getName();
						if (i != (size - 1)) {
							name += ".";
						}
					}

					Field lastField = responseField.get(size - 1);
					com.gcloud.header.api.model.ApiModel model = new com.gcloud.header.api.model.ApiModel();
					ApiModel am = lastField.getAnnotation(ApiModel.class);
					if (am != null) {
						model.setRequire(am.require());
						model.setExample(am.example());
						model.setDescription(am.description());
						model.setType(lastField.getType().getSimpleName());
						model.setName(name);
						amList.add(model);
					}
				}

			}
		}
		return amList;
	}
	
	//获取文档信息的结�? 版本--主模�?--子模�?--接口列表--接口详情
	private Map<String, Map<String, Map<String, List<ApiBody>>>> getDocDataTypeWithVersion() throws ClassNotFoundException {
		//版本结构
		Map<String, Map<String, Map<String, List<ApiBody>>>> versionInfo = new HashMap<>();
		Iterator<ApiVersion> it = versionHandlers.keySet().iterator();
		while(it.hasNext()) {
			ApiVersion version = it.next();
			
			//主模块结�?
			Map<String, Map<String, List<ApiBody>>> moduleInfo = new HashMap<>();
			
			Map<Module, Map<SubModule, Map<String,Class>>> moduleMap = versionHandlers.get(version);
			Iterator<Module> moduleIt = moduleMap.keySet().iterator();
			while(moduleIt.hasNext()){
				Module module = moduleIt.next();
				String moduleName = (StringUtils.isNotBlank(module.getCnName()) ? module.getCnName() : module.name());
				
				//子模块结�?
				Map<String, List<ApiBody>> subModuleInfo = new HashMap<>();
				
				Map<SubModule,Map<String,Class>> subMap = moduleMap.get(module);
				Iterator<SubModule> subIt = subMap.keySet().iterator();
				while(subIt.hasNext()){
					SubModule subModule = subIt.next();
					
					String subModuleName = null;
					if(subModule != SubModule.NONE){
						subModuleName = (StringUtils.isNotBlank(subModule.getCnName()) ? subModule.getCnName() : subModule.name());
					}
					
					//初始化子模块
					List<ApiBody> interfaceList = subModuleInfo.get(subModuleName);
					if(null == interfaceList) {
						interfaceList = new ArrayList<>();
						subModuleInfo.put(subModuleName, interfaceList);
					}
					
					Map<String,Class> actionMap = subMap.get(subModule);
					Iterator<String> actionIt = actionMap.keySet().iterator();
					while(actionIt.hasNext()){
						String action = actionIt.next();
						Class<?> clazz = actionMap.get(action);
						ApiHandler apiHandler = clazz.getAnnotation(ApiHandler.class);
						
						//接口信息
						ApiBody interfaceInfo = new ApiBody();
						
						String interfaceName = null;
						if(StringUtils.isNotBlank(apiHandler.name())){
							interfaceName = apiHandler.name();
						}else {
							GcLog gclog = clazz.getAnnotation(GcLog.class);
							if(gclog!=null) {
								interfaceName = gclog.taskExpect();
							}
						}
						
						//接口url
						String interfaceUrl = "/" + module.name().toLowerCase() + "/" + action + ".do";

						//请求参数
						List<com.gcloud.header.api.model.ApiModel> requests = new ArrayList<>();
						
						//返回参数
						List<com.gcloud.header.api.model.ApiModel> responses = new ArrayList<>();
						
						Type types = clazz.getGenericSuperclass();
				        Type[] genericType = ((ParameterizedType) types).getActualTypeArguments();
				        if(genericType.length == 2){
				        	//请求参数
				        	Class reqClazz= Class.forName(genericType[0].getTypeName());
				        	Field[] reqFields = ReflectionUtils.getDeclaredField(reqClazz);
				        	for(Field field:reqFields){
				        		ApiModel apiModel = field.getAnnotation(ApiModel.class);
				        		if(apiModel != null){
				        			com.gcloud.header.api.model.ApiModel request = new com.gcloud.header.api.model.ApiModel();
				        			request.setName(field.getName());
				        			request.setType(apiModel.type());
				        			request.setRequire(apiModel.require() ? true : false);
				        			request.setExample(apiModel.example());
				        			request.setDescription(apiModel.description());
				        			requests.add(request);
				        		}
				        	}
				        	
				        	//返回参数
				        	Class respClazz= Class.forName(genericType[1].getTypeName());
				        	Field[] respFields=ReflectionUtils.getDeclaredField(respClazz);
				        	List<com.gcloud.header.api.model.ApiModel> apiModels = addModel(respFields);
				        	for(com.gcloud.header.api.model.ApiModel apiModel:apiModels){
				        		com.gcloud.header.api.model.ApiModel response = new com.gcloud.header.api.model.ApiModel();
				        		response.setName(apiModel.getName());
				        		response.setType(apiModel.getType());
				        		response.setExample(apiModel.getExample());
				        		response.setDescription(apiModel.getDescription());
				        		responses.add(response);
				        	}
				        }
				        
				        //配置接口信息
				        interfaceInfo.setName(interfaceName);
				        interfaceInfo.setUrl(interfaceUrl);
				        interfaceInfo.setRequestParam(requests);
				        interfaceInfo.setResponse(responses);
				        interfaceList.add(interfaceInfo);
					}
					//配置子模�?
					subModuleInfo.put(subModuleName, interfaceList);
				}
				//配置主模�?
				moduleInfo.put(moduleName, subModuleInfo);
			}
			//配置版本
			versionInfo.put(version.name(), moduleInfo);
		}
		return versionInfo;
	}
	
	private void createDocWithVersion(String filePath, Map<String,Map<String, Map<String, List<ApiBody>>>> api) throws IOException {
		final String TITLE = "Gcloud8 API 文档";
		final String TITLE_COLOR = "000000";
		final int TITLE_FONT_SIZE = 20;
		final int VERSION_FONT_SIZE = 20;
		final int MODULE_FONT_SIZE = 18;
		final int SUB_MODULE_FONT_SIZE = 16;
		final int INTERFACE_FONT_SIZE = 14;
		final int TXT_DESP_FONT_SIZE = 12;
		final int DESP_FONT_SIZE = 10;
		final int TXT_URI_FONT_SIZE = 12;
		final int URI_FONT_SIZE = 10;
		final int TXT_REQ_FONT_SIZE = 12;
		final int TXT_RES_FONT_SIZE = 12;
		//请求参数表的宽度
		final int REQ_TAB_W = 9072;
		//返回参数表的宽度
		final int RES_TAB_W = 9072;
		
		
		XWPFDocument document = new XWPFDocument();
		
		FileOutputStream out = new FileOutputStream(new File(filePath));
		addCustomHeadingStyle(document, "标题1", 1);
		addCustomHeadingStyle(document, "标题2", 2);
		addCustomHeadingStyle(document, "标题3", 3);
		addCustomHeadingStyle(document, "标题4", 4);
		addCustomHeadingStyle(document, "标题5", 5);
		
		//设置标题段落格式
		XWPFParagraph titleParagraph = document.createParagraph();
		titleParagraph.setStyle("标题1");
		titleParagraph.setAlignment(ParagraphAlignment.CENTER);
		
		XWPFRun titleParagraphRun = titleParagraph.createRun();
		titleParagraphRun.setText(TITLE);
		titleParagraphRun.setColor(TITLE_COLOR);
		titleParagraphRun.setFontSize(TITLE_FONT_SIZE);
		
		for (Map.Entry<String, Map<String, Map<String, List<ApiBody>>>> version : api.entrySet()) {
			if(api.size() > 1) {
				XWPFParagraph versionParagraph = document.createParagraph();
				versionParagraph.setStyle("标题2");
				XWPFRun versionParagraphRun = versionParagraph.createRun();
				//版本标题名称
				versionParagraphRun.setText(String.format("版本�?%s", version.getKey()));
				versionParagraphRun.setFontSize(VERSION_FONT_SIZE);
				versionParagraphRun.setBold(true);
			}
			
			int moduleNum = 1;
			for(Map.Entry<String, Map<String, List<ApiBody>>> module: version.getValue().entrySet()) {
				XWPFParagraph moduleParagraph = document.createParagraph();
				moduleParagraph.setStyle("标题3");
				XWPFRun moduleParagraphRun = moduleParagraph.createRun();
				//主模块的标题名称
				moduleParagraphRun.setText(String.format("%s�?%s", moduleNum, module.getKey()));
				moduleParagraphRun.setFontSize(MODULE_FONT_SIZE);
				moduleParagraphRun.setBold(true);
				
				int subMoudleNum = 1;
				for(Map.Entry<String, List<ApiBody>> subModule: module.getValue().entrySet()) {
					String subModuleName = subModule.getKey();
					boolean subModuleNameNull = true;
					if(StringUtils.isNotBlank(subModuleName)) {
						XWPFParagraph subModuleParagraph = document.createParagraph();
						subModuleParagraph.setStyle("标题4");
						XWPFRun subModuleParagraphRun = subModuleParagraph.createRun();
						//子模块的标题名称
						subModuleParagraphRun.setText(String.format("%s.%s�?%s", moduleNum, subMoudleNum, subModule.getKey()));
						subModuleParagraphRun.setFontSize(SUB_MODULE_FONT_SIZE);
						subModuleParagraphRun.setBold(true);
						subModuleNameNull = false;
					}
					
					int apiNum = 1;
					for(ApiBody interfaceInfo: subModule.getValue()) {
						XWPFParagraph interfaceParagraph = document.createParagraph();
						interfaceParagraph.setStyle(subModuleNameNull ? "标题4": "标题5");
						XWPFRun interfaceParagraphRun = interfaceParagraph.createRun();
						//接口标题名称
						//TODO 优化
						String interfaceName = subModuleNameNull ? String.format("%s.%s�?%s", moduleNum, apiNum, StringUtils.isNotBlank(interfaceInfo.getName()) ? interfaceInfo.getName() : interfaceInfo.getUrl()) 
								: String.format("%s.%s.%s�?%s", moduleNum, subMoudleNum, apiNum,  StringUtils.isNotBlank(interfaceInfo.getName()) ? interfaceInfo.getName(): interfaceInfo.getUrl());
						interfaceParagraphRun.setText(interfaceName);
						interfaceParagraphRun.setFontSize(INTERFACE_FONT_SIZE);
						interfaceParagraphRun.setBold(true);
						
						if(StringUtils.isNotBlank(interfaceInfo.getDescription())) {
							setTitleAndBody(document, "描述", interfaceInfo.getDescription(), TXT_DESP_FONT_SIZE, DESP_FONT_SIZE);
						}
						setTitleAndBody(document, "URI", interfaceInfo.getUrl(), TXT_URI_FONT_SIZE, URI_FONT_SIZE);
						
						//请求参数字样
						XWPFParagraph txtReqParagraph = document.createParagraph();
						XWPFRun txtReqParagraphRun = txtReqParagraph.createRun();
						txtReqParagraphRun.setText("请求参数");
						txtReqParagraphRun.setFontSize(TXT_REQ_FONT_SIZE);
						titleParagraphRun.setBold(true);
						
						//请求参数列表的配�?
						XWPFTable reqTable = document.createTable();
						CTTblWidth reqTableWidth = reqTable.getCTTbl().addNewTblPr().addNewTblW();
						reqTableWidth.setType(STTblWidth.DXA);
						reqTableWidth.setW(BigInteger.valueOf(REQ_TAB_W));
						
						//请求参数表头
						//"参数","类型","必须?","例子","描述"
						XWPFTableRow reqTabRowOne = reqTable.getRow(0);
						reqTabRowOne.getCell(0).setText("参数");
						reqTabRowOne.addNewTableCell().setText("类型");
						reqTabRowOne.addNewTableCell().setText("必须?");
						reqTabRowOne.addNewTableCell().setText("例子");
						reqTabRowOne.addNewTableCell().setText("描述");
						
						//请求参数数据填充
						if(interfaceInfo.getRequestParam() != null && !interfaceInfo.getRequestParam().isEmpty()) {
							for(com.gcloud.header.api.model.ApiModel req : interfaceInfo.getRequestParam()){
								XWPFTableRow reqTableRow = reqTable.createRow();
								reqTableRow.getCell(0).setText(req.getName());
								reqTableRow.getCell(1).setText(req.getType());
								reqTableRow.getCell(2).setText(req.isRequire() ? "�?": "�?");
								reqTableRow.getCell(3).setText(req.getExample());
								reqTableRow.getCell(4).setText(req.getDescription());
							}
						} else {
							XWPFTableRow reqTableRow = reqTable.createRow();
							reqTableRow.getCell(0).setText("�?");
							reqTableRow.getCell(1).setText("�?");
							reqTableRow.getCell(2).setText("�?");
							reqTableRow.getCell(3).setText("�?");
							reqTableRow.getCell(4).setText("�?");
						}
						
						//requestBody类型，获取数据的时�?�没有这�?
						
						//返回参数字样
						XWPFParagraph txtResParagraph = document.createParagraph();
						XWPFRun txtResParagraphRun = txtResParagraph.createRun();
						txtResParagraphRun.setText("响应");
						txtResParagraphRun.setFontSize(TXT_RES_FONT_SIZE);
						txtResParagraphRun.setBold(true);
						
						//返回参数列表的配�?
						XWPFTable resTable = document.createTable();
						CTTblWidth resTableWidth = resTable.getCTTbl().addNewTblPr().addNewTblW();
						resTableWidth.setType(STTblWidth.DXA);
						resTableWidth.setW(BigInteger.valueOf(RES_TAB_W));
						
						//返回参数表头
						//"参数","类型","例子","描述"
						XWPFTableRow resTabRowOne = resTable.getRow(0);
						resTabRowOne.getCell(0).setText("参数");
						resTabRowOne.addNewTableCell().setText("类型");
						resTabRowOne.addNewTableCell().setText("例子");
						resTabRowOne.addNewTableCell().setText("描述");
						
						//返回参数数据填充
						if(interfaceInfo.getResponse() != null && !interfaceInfo.getResponse().isEmpty()) {
							for (com.gcloud.header.api.model.ApiModel res : interfaceInfo.getResponse()) {
								XWPFTableRow resTableRow = resTable.createRow();
								resTableRow.getCell(0).setText(res.getName());
								resTableRow.getCell(1).setText(res.getType());
								resTableRow.getCell(2).setText(res.getExample());
								resTableRow.getCell(3).setText(res.getDescription());
							}
						} else {
							XWPFTableRow resTableRow = resTable.createRow();
							resTableRow.getCell(0).setText("�?");
							resTableRow.getCell(1).setText("�?");
							resTableRow.getCell(2).setText("�?");
							resTableRow.getCell(3).setText("�?");
						}
						
						// 换行
		                XWPFParagraph paragraph = document.createParagraph();
		                XWPFRun paragraphRun = paragraph.createRun();
		                paragraphRun.setText("\n\n");
		                paragraphRun.setFontSize(16);
						apiNum ++;
					}
					subMoudleNum ++;
				}
				moduleNum ++;
			}
		}
		
		document.write(out);
		out.close();
	}
	
	private void addCustomHeadingStyle(XWPFDocument docxDocument, String strStyleId, int headingLevel) {
        CTStyle ctStyle = CTStyle.Factory.newInstance();
        ctStyle.setStyleId(strStyleId);

        CTString styleName = CTString.Factory.newInstance();
        styleName.setVal(strStyleId);
        ctStyle.setName(styleName);

        CTDecimalNumber indentNumber = CTDecimalNumber.Factory.newInstance();
        indentNumber.setVal(BigInteger.valueOf(headingLevel));

        // lower number > style is more prominent in the formats bar
        ctStyle.setUiPriority(indentNumber);

        CTOnOff onoffnull = CTOnOff.Factory.newInstance();
        ctStyle.setUnhideWhenUsed(onoffnull);

        // style shows up in the formats bar
        ctStyle.setQFormat(onoffnull);

        // style defines a heading of the given level
        CTPPr ppr = CTPPr.Factory.newInstance();
        ppr.setOutlineLvl(indentNumber);
        ctStyle.setPPr(ppr);

        XWPFStyle style = new XWPFStyle(ctStyle);

        // is a null op if already defined
        XWPFStyles styles = docxDocument.createStyles();

        style.setType(STStyleType.PARAGRAPH);
        styles.addStyle(style);

    }
	
	private void setTitleAndBody(XWPFDocument document, String title, String body, int titleFontSize, int bodyFontSize) {
		setTitle(document, title, titleFontSize);
		setBody(document, body, bodyFontSize);
	}
	
	private void setTitle(XWPFDocument document, String title, int titleFontSize) {
		XWPFParagraph titleParagraph = document.createParagraph();
		XWPFRun titleParagraphRun = titleParagraph.createRun();
		titleParagraphRun.setText(title);
		titleParagraphRun.setFontSize(titleFontSize);
		titleParagraphRun.setBold(true);
	}
	
	private void setBody(XWPFDocument document, String body, int bodyFontSize) {
		XWPFParagraph bodyParagraph = document.createParagraph();
		XWPFRun bodyParagraphRun = bodyParagraph.createRun();
		bodyParagraphRun.setText(body);
		bodyParagraphRun.setFontSize(bodyFontSize);
	}
	
	private ApiVersion[] getRepeat(ApiVersion[] standard, ApiVersion[] array) {
		List<ApiVersion> resultList = new ArrayList<>();
		List<ApiVersion> tempStandard = Arrays.asList(standard);
		for (ApiVersion version : array) {
			if(tempStandard.contains(version)) {
				resultList.add(version);
			}
		}
		ApiVersion[] cover = new ApiVersion[resultList.size()];
		ApiVersion[] result = resultList.toArray(cover);
		return result;
	}
}