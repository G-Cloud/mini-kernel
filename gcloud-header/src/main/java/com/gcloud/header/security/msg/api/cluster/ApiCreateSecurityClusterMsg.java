package com.gcloud.header.security.msg.api.cluster;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.security.msg.model.CreateClusterInfoParams;
import com.gcloud.header.security.msg.model.CreateClusterObjectParams;
import com.gcloud.header.security.msg.model.CreateHaInfoParams;
import com.gcloud.header.security.msg.model.CreateNetworkParams;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiCreateSecurityClusterMsg extends ApiMessage {

    @Override
    public Class replyClazz() {
        return ApiCreateSecurityClusterReplyMsg.class;
    }

    private static final long serialVersionUID = 1L;

    @ApiModel(description = "安全集群名称")
    private String name;
    @ApiModel(description = "描述")
    private String description;

    @ApiModel(description = "安全集群信息")
    private CreateClusterInfoParams createInfo;

    @ApiModel(description = "外网参数")
    private CreateNetworkParams outerNet;
    @ApiModel(description = "")
    private CreateNetworkParams managementNet;
    @ApiModel(description = "")
    private CreateNetworkParams protectionNet;

    @ApiModel(description = "防火墙参�?")
    private CreateClusterObjectParams firewall;
    @ApiModel(description = "")
    private CreateClusterObjectParams fortress;
    @ApiModel(description = "waf")
    private CreateClusterObjectParams waf;
    @ApiModel(description = "isms")
    private CreateClusterObjectParams isms;

    @ApiModel(description = "HA")
    private Boolean ha;
    @ApiModel(description = "")
    private CreateHaInfoParams haInfo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CreateClusterInfoParams getCreateInfo() {
        return createInfo;
    }

    public void setCreateInfo(CreateClusterInfoParams createInfo) {
        this.createInfo = createInfo;
    }

    public CreateNetworkParams getOuterNet() {
        return outerNet;
    }

    public void setOuterNet(CreateNetworkParams outerNet) {
        this.outerNet = outerNet;
    }

    public CreateNetworkParams getManagementNet() {
        return managementNet;
    }

    public void setManagementNet(CreateNetworkParams managementNet) {
        this.managementNet = managementNet;
    }

    public CreateNetworkParams getProtectionNet() {
        return protectionNet;
    }

    public void setProtectionNet(CreateNetworkParams protectionNet) {
        this.protectionNet = protectionNet;
    }

    public CreateClusterObjectParams getFirewall() {
        return firewall;
    }

    public void setFirewall(CreateClusterObjectParams firewall) {
        this.firewall = firewall;
    }

    public CreateClusterObjectParams getFortress() {
        return fortress;
    }

    public void setFortress(CreateClusterObjectParams fortress) {
        this.fortress = fortress;
    }

    public CreateClusterObjectParams getWaf() {
        return waf;
    }

    public void setWaf(CreateClusterObjectParams waf) {
        this.waf = waf;
    }

    public CreateClusterObjectParams getIsms() {
        return isms;
    }

    public void setIsms(CreateClusterObjectParams isms) {
        this.isms = isms;
    }

    public Boolean getHa() {
        return ha;
    }

    public void setHa(Boolean ha) {
        this.ha = ha;
    }

    public CreateHaInfoParams getHaInfo() {
        return haInfo;
    }

    public void setHaInfo(CreateHaInfoParams haInfo) {
        this.haInfo = haInfo;
    }
}