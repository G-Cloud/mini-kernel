package com.gcloud.header.network.msg.api;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class UpdateNetcardQosMsg extends ApiMessage {
	
	@ApiModel(description = "网卡ID", require = true)
	@NotBlank(message = "0080501::端口ID不能为空")
	private String networkInterfaceId;
	@ApiModel(description = "入口带宽")
//	@NotNull(message = "0080503::入口速度不能为空")
//	@Min(value = 0, message = "0080504::入口速度要大�?0")
	private Integer ingress;//单位mb/s
	@ApiModel(description = "出口带宽")
//	@NotNull(message = "0080505::出口速度不能为空")
//	@Min(value = 0, message = "0080506::出口速度要大�?0")
	private Integer outgress;
	
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiReplyMessage.class;
	}

	public String getNetworkInterfaceId() {
		return networkInterfaceId;
	}

	public void setNetworkInterfaceId(String networkInterfaceId) {
		this.networkInterfaceId = networkInterfaceId;
	}

	public Integer getIngress() {
		return ingress;
	}

	public void setIngress(Integer ingress) {
		this.ingress = ingress;
	}

	public Integer getOutgress() {
		return outgress;
	}

	public void setOutgress(Integer outgress) {
		this.outgress = outgress;
	}
	
}