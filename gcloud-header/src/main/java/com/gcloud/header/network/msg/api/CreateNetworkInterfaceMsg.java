package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.common.RegExp;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class CreateNetworkInterfaceMsg extends ApiMessage {

	private static final long serialVersionUID = 1L;

	@ApiModel(description = "子网ID", require = true)
	@NotBlank(message = "0080101::子网ID不能为空")
	private String vSwitchId;
	@ApiModel(description = "IP地址")
	@Pattern(regexp = RegExp.IPV4, message = "0080105::请输入正确的IP")
	private String primaryIpAddress;
	@ApiModel(description = "安全组ID", require = true)
//	@NotBlank(message = "0080102::安全组ID不能为空")
	private String securityGroupId;
	@ApiModel(description = "网卡名称")
    @Length(max = 255, message = "0080106::名称长度不能大于255")
	private String networkInterfaceName;
	@ApiModel(description = "描述")
    @Length(max = 255, message = "0080107::描述长度不能大于255")
	private String description;

	@ApiModel(description = "端口关联的设备ID")
	private String deviceId;
	@ApiModel(description = "端口关联的设备类�?")
	private String deviceOwner;
	@ApiModel(description = "绑定的节�?")
	private String bindingHost;
	@ApiModel(description = "是否启用安全�?")
	private Boolean portSecurityEnabled;


	public String getvSwitchId() {
		return vSwitchId;
	}


	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}


	public String getPrimaryIpAddress() {
		return primaryIpAddress;
	}


	public void setPrimaryIpAddress(String primaryIpAddress) {
		this.primaryIpAddress = primaryIpAddress;
	}


	public String getSecurityGroupId() {
		return securityGroupId;
	}


	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}


	public String getNetworkInterfaceName() {
		return networkInterfaceName;
	}


	public void setNetworkInterfaceName(String networkInterfaceName) {
		this.networkInterfaceName = networkInterfaceName;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceOwner() {
		return deviceOwner;
	}

	public void setDeviceOwner(String deviceOwner) {
		this.deviceOwner = deviceOwner;
	}

	public String getBindingHost() {
		return bindingHost;
	}

	public void setBindingHost(String bindingHost) {
		this.bindingHost = bindingHost;
	}

	public Boolean getPortSecurityEnabled() {
		return portSecurityEnabled;
	}

	public void setPortSecurityEnabled(Boolean portSecurityEnabled) {
		this.portSecurityEnabled = portSecurityEnabled;
	}

	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return CreateNetworkInterfaceReplyMsg.class;
	}

}