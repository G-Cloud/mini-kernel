package com.gcloud.header.network.msg.api;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.header.ApiCreateMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class CreateExternalNetworkMsg extends ApiCreateMessage{

	@ApiModel(description="网络名称", require=true)
	@Length(min=2, max = 20, message = "0160101::外部网络名称不能为空")
	private String networkName;
	@ApiModel(description="网络模式,FLAT、VLAN;", require=true)
	@NotBlank(message="0160102::网络模式不能为空")
	private String networkType;// LOCAL,FLAT,VLAN,GRE
	@ApiModel(description="物理网络", require=true)
	@NotBlank(message="0160103::物理网络不能为空")
	private String physicalNetwork;
	@ApiModel(description="segment ID，vlan模式时必�?")
	private Integer segmentId;
	
	public String getNetworkName() {
		return networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getPhysicalNetwork() {
		return physicalNetwork;
	}

	public void setPhysicalNetwork(String physicalNetwork) {
		this.physicalNetwork = physicalNetwork;
	}

	public Integer getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(Integer segmentId) {
		this.segmentId = segmentId;
	}

	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return CreateExternalNetworkReplyMsg.class;
	}
	
	
}