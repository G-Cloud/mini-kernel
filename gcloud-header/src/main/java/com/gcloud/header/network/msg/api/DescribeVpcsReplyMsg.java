package com.gcloud.header.network.msg.api;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.DescribeVpcsResponse;
import com.gcloud.header.network.model.VpcsItemType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeVpcsReplyMsg extends PageReplyMessage<VpcsItemType> {
	
	@ApiModel(description = "vpc信息")
	private DescribeVpcsResponse vpcs;

	public DescribeVpcsResponse getVpcs() {
		return vpcs;
	}

	public void setVpcs(DescribeVpcsResponse vpcs) {
		this.vpcs = vpcs;
	}

	@Override
	public void setList(List<VpcsItemType> list) {
		// TODO Auto-generated method stub
		vpcs=new DescribeVpcsResponse();
		vpcs.setVpc(list);
	}
	
}