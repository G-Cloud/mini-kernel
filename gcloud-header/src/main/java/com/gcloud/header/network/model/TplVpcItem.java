package com.gcloud.header.network.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class TplVpcItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="vpc ID")
	private String vpcId;
	@ApiModel(description="vpc 名称")
	private String vpcName;
	@ApiModel(description="路由信息")
	private TplVRouterSet vRouter;
	@ApiModel(description = "交换机信息集�?")
	private TplVSwitchItems vSwitchItemItems;
	public String getVpcId() {
		return vpcId;
	}
	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}
	public String getVpcName() {
		return vpcName;
	}
	public void setVpcName(String vpcName) {
		this.vpcName = vpcName;
	}
	public TplVRouterSet getvRouter() {
		return vRouter;
	}
	public void setvRouter(TplVRouterSet vRouter) {
		this.vRouter = vRouter;
	}
	public TplVSwitchItems getvSwitchItemItems() {
		return vSwitchItemItems;
	}
	public void setvSwitchItemItems(TplVSwitchItems vSwitchItemItems) {
		this.vSwitchItemItems = vSwitchItemItems;
	}

}