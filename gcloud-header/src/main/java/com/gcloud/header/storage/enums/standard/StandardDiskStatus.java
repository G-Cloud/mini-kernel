package com.gcloud.header.storage.enums.standard;

import com.gcloud.header.storage.enums.VolumeStatus;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum StandardDiskStatus {

    AVAILABLE("Available", "可使�?", Arrays.asList(VolumeStatus.AVAILABLE)),
    ATTACHING("Attaching", "挂载�?", Arrays.asList(VolumeStatus.ATTACHING)),
    TASKING("Tasking", "任务�?", Arrays.asList(VolumeStatus.CREATING_BACKUP, VolumeStatus.RESIZING, VolumeStatus.RESTORING_BACKUP, VolumeStatus.UPLOADING, VolumeStatus.DOWNLOADING)),
    ERROR("Error", "错误", Arrays.asList(VolumeStatus.ERROR, VolumeStatus.ERROR_DELETING, VolumeStatus.ERROR_RESTORING, VolumeStatus.UNRECOGNIZED)),
    REINITING("Reiniting", "初始化中", null),
    DELETING("Deleting", "删除�?", Arrays.asList(VolumeStatus.DELETING)),
    INUSED("In_use", "已挂�?", Arrays.asList(VolumeStatus.IN_USE)),
    DETACHING("Detaching", "卸载�?", Arrays.asList(VolumeStatus.DETACHING)),
    CREATING("Creating", "创建�?", Arrays.asList(VolumeStatus.CREATING)),
    DELETED("Deleted", "已删�?", null);

    private String value;
    private String cnName;
    private List<VolumeStatus> gcStatus;

    StandardDiskStatus(String value, String cnName, List<VolumeStatus> gcStatus) {
        this.value = value;
        this.cnName = cnName;
        this.gcStatus = gcStatus;
    }

    public List<String> getGcStatusValues(){
        if(gcStatus == null){
            return null;
        }

        List<String> resultStatus = new ArrayList<>();
        gcStatus.forEach(s -> resultStatus.add(s.value()));
        return resultStatus;
    }

    public static String standardStatus(String gcStatusStr){

        if(StringUtils.isBlank(gcStatusStr)){
            return gcStatusStr;
        }

        for(StandardDiskStatus status : StandardDiskStatus.values()){
            if(status.gcStatus == null || status.getGcStatus().size() == 0){
                continue;
            }
            for(VolumeStatus gcStatus : status.getGcStatus()){
                if(gcStatus.value().equals(gcStatusStr)){
                    return status.getValue();
                }
            }
        }

        return null;
    }

    public static StandardDiskStatus value(String value){
        return Arrays.stream(StandardDiskStatus.values()).filter(s -> s.getValue().equals(value)).findFirst().orElse(null);
    }

    public String getValue() {
        return value;
    }

    public String getCnName() {
        return cnName;
    }

    public List<VolumeStatus> getGcStatus() {
        return gcStatus;
    }
}