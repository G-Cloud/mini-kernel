package com.gcloud.header.storage.msg.api.snapshot;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.StorageErrorCodes;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ApiCreateSnapshotMsg extends ApiMessage {

    private static final long serialVersionUID = 1L;

    @Override
    public Class replyClazz() {
        return ApiCreateSnapshotReplyMsg.class;
    }

    @ApiModel(description = "磁盘ID", require = true)
    @NotBlank(message = StorageErrorCodes.INPUT_DISK_ID_ERROR)
    private String diskId;
    @ApiModel(description = "快照名称，最大长度为255", require = true)
    @Length(max = 255, message = StorageErrorCodes.INPUT_SNAPSHOT_NAME_ERROR)
    private String snapshotName;
    @ApiModel(description = "描述�? �?大长度为255")
    @Length(max = 255, message = StorageErrorCodes.INPUT_SNAPSHOT_DESCRIPTION_ERROR)
    private String description;

    public String getDiskId() {
        return diskId;
    }

    public void setDiskId(String diskId) {
        this.diskId = diskId;
    }

    public String getSnapshotName() {
        return snapshotName;
    }

    public void setSnapshotName(String snapshotName) {
        this.snapshotName = snapshotName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}