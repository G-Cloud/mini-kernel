package com.gcloud.header.storage.enums;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum StoragePoolDriver {

    FILE,
    RBD,
    LVM;

    public static StoragePoolDriver get(String value) {
        if (value != null) {
            for (StoragePoolDriver driver : StoragePoolDriver.values()) {
                if (driver.name().equals(value)) {
                    return driver;
                }
            }
        }
        return null;
    }
    
    public String getValue() {
        return name().toLowerCase();
    }

}