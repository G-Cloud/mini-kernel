package com.gcloud.header.storage.msg.api.pool;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ApiDescribeDiskCategoriesMsg extends ApiPageMessage {

    private static final long serialVersionUID = 1L;

    @Override
    public Class replyClazz() {
        return ApiDescribeDiskCategoriesReplyMsg.class;
    }

    @ApiModel(description = "可用区ID")
    private String zoneId;
    @ApiModel(description = "磁盘类型名称")
    private String diskCategoryName;
    @ApiModel(description = "存储类型, local:本地存储, distributed:分布式存�?, central:集中存储")
    private String storageType;
    @ApiModel(description = "节点名称")
    private String hostname;
    @ApiModel(description = "查找此节点可用的磁盘类型")
    private String availableForHost;

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

	public String getDiskCategoryName() {
		return diskCategoryName;
	}

	public void setDiskCategoryName(String diskCategoryName) {
		this.diskCategoryName = diskCategoryName;
	}

	public String getStorageType() {
		return storageType;
	}

	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

    public String getAvailableForHost() {
        return availableForHost;
    }

    public void setAvailableForHost(String availableForHost) {
        this.availableForHost = availableForHost;
    }
}