package com.gcloud.header.enums;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum ProviderType {

    GCLOUD(0),
    CINDER(1),
    GLANCE(2),
    NEUTRON(3);

    private int value;

    ProviderType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ProviderType get(Integer value) {
        if (value != null) {
            for (ProviderType type : ProviderType.values()) {
                if (type.getValue() - value == 0) {
                    return type;
                }
            }
        }
        return null;
    }

}