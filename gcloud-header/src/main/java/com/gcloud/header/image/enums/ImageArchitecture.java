package com.gcloud.header.image.enums;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum ImageArchitecture {

    X86_64, I386, I686;

    public String value(){
        return name().toLowerCase();
    }

    public static ImageArchitecture value(String value){
        return Arrays.stream(ImageArchitecture.values()).filter(type -> type.value().equals(value)).findFirst().orElse(null);
    }

}