package com.gcloud.header.image.enums;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum ImageProvider {

    GCLOUD(0, "gcloud"),
    GLANCE(1, "glance");

    private int value;
    private String enValue;

    ImageProvider(int value, String enValue) {
        this.value = value;
        this.enValue = enValue;
    }

    public int getValue() {
        return value;
    }

    public String getEnValue() {
        return enValue;
    }

    public static ImageProvider value(String enValue){
        return Arrays.stream(ImageProvider.values()).filter(s -> s.getEnValue().equals(enValue)).findFirst().orElse(null);
    }

}