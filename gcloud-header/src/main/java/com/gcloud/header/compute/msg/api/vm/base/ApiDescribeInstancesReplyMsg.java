package com.gcloud.header.compute.msg.api.vm.base;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.compute.msg.api.model.DescribeInstanceAttributesTypeResponse;
import com.gcloud.header.compute.msg.api.model.InstanceAttributesType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeInstancesReplyMsg extends PageReplyMessage<InstanceAttributesType>{
	@ApiModel(description = "云服务器列表")
	private DescribeInstanceAttributesTypeResponse instances;
	
	@Override
	public void setList(List<InstanceAttributesType> list) {
		instances = new DescribeInstanceAttributesTypeResponse();
		instances.setInstance(list);
	}

	public DescribeInstanceAttributesTypeResponse getInstances() {
		return instances;
	}

	public void setInstances(DescribeInstanceAttributesTypeResponse instances) {
		this.instances = instances;
	}
	
}