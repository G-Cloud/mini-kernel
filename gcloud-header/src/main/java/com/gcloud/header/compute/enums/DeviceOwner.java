package com.gcloud.header.compute.enums;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum DeviceOwner {
	COMPUTE("compute:node", true),
	FOREIGN("network:foreign", true),
	ZX_FIREWALL("compute:zx_firewall", true),
	DHCP("network:dhcp", false),
	FLOATINGIP("network:floatingip", false),
	ROUTER("network:router_interface", false),
	HA_ROUTER("network:ha_router_replicated_interface", false),
	LOADBALANCER("neutron:LOADBALANCERV2", false),
	GATEWAY("network:router_gateway", false),
	;

	
	private String value;
	private Boolean eni; //是否是网�?

	DeviceOwner(String value, Boolean eni) {
		this.value = value;
		this.eni = eni;
	}

	public String getValue() {
		return value;
	}

	public Boolean getEni() {
		return eni;
	}

	public static DeviceOwner value(String value){
		if(StringUtils.isBlank(value)){
			return null;
		}
		return Arrays.stream(DeviceOwner.values()).filter(d -> d.getValue().equals(value)).findFirst().orElse(null);
	}

	public static List<DeviceOwner> eniDeviceOwners(){
		return Arrays.stream(DeviceOwner.values()).filter(d -> d.eni).collect(Collectors.toList());
	}

	public static List<String> eniDeviceOwnerValues(){
		return Arrays.stream(DeviceOwner.values()).filter(d -> d.eni).map(DeviceOwner::getValue).collect(Collectors.toList());
	}
}