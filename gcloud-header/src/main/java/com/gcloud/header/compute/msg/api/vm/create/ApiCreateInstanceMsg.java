package com.gcloud.header.compute.msg.api.vm.create;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.compute.msg.api.model.DiskInfo;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiCreateInstanceMsg extends ApiMessage {

    @Override
    public Class replyClazz() {
        return ApiCreateInstanceReplyMsg.class;
    }

    @ApiModel(description = "系统盘名�?")
    private String systemDiskName;

    @ApiModel(description = "镜像ID", require = true)
    //@NotBlank(message = "0010101::镜像ID不能为空")  //暂时iso和image创建用同�?个接�?
    private String imageId;
    @ApiModel(description = "实例类型ID")
    //@NotBlank(message = "0010102::实例资源规格不能为空")
    private String instanceType;
    @ApiModel(description = "实例名称", require = true)
    @NotBlank(message = "0010103::实例名称不能为空")
    @Length(min = 4, max = 20, message = "0010104::名称长度�?4-20")
    private String instanceName;
    @ApiModel(description = "实例主机�?")
    @Length(min = 2, max = 15, message = "0010105::主机名长度为2-15")
    private String hostName;
    @ApiModel(description = "实例密码")
    @Length(min = 8, max = 20, message = "0010106::密码长度�?8-20")
    private String password;
    @ApiModel(description = "虚拟交换机ID")
    private String vSwitchId;
    @ApiModel(description = "实例私网 IP 地址")
    private String privateIpAddress;
    @ApiModel(description = "数据�?")
    private List<DiskInfo> dataDisk;
    @ApiModel(description = "系统盘大�?", require = true)
    private Integer systemDiskSize;
    @ApiModel(description = "系统盘的磁盘种类")
    private String systemDiskCategory;
    @ApiModel(description = "可用地域")
    private String zoneId;

    @ApiModel(description = "安全�?")
    private String securityGroupId;
    @ApiModel(description = "映像ID")
    private String isoId;

    @ApiModel(description = "云服务器创建的节�?")
    private String createHost;

    // 常量
    private String systemDiskType = "system";
    private String DataDiskType = "data";

    public String getIsoId() {
		return isoId;
	}

	public void setIsoId(String isoId) {
		this.isoId = isoId;
	}

	public String getSystemDiskType() {
        return systemDiskType;
    }

    public void setSystemDiskType(String systemDiskType) {
        this.systemDiskType = systemDiskType;
    }

    public String getDataDiskType() {
        return DataDiskType;
    }

    public void setDataDiskType(String dataDiskType) {
        DataDiskType = dataDiskType;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getInstanceType() {
        return instanceType;
    }

    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getvSwitchId() {
        return vSwitchId;
    }

    public void setvSwitchId(String vSwitchId) {
        this.vSwitchId = vSwitchId;
    }

    public String getPrivateIpAddress() {
        return privateIpAddress;
    }

    public void setPrivateIpAddress(String privateIpAddress) {
        this.privateIpAddress = privateIpAddress;
    }

    public List<DiskInfo> getDataDisk() {
        return dataDisk;
    }

    public void setDataDisk(List<DiskInfo> dataDisk) {
        this.dataDisk = dataDisk;
    }

    public String getSystemDiskName() {
        return systemDiskName;
    }

    public void setSystemDiskName(String systemDiskName) {
        this.systemDiskName = systemDiskName;
    }

    public Integer getSystemDiskSize() {
        return systemDiskSize;
    }

    public void setSystemDiskSize(Integer systemDiskSize) {
        this.systemDiskSize = systemDiskSize;
    }

    public String getSystemDiskCategory() {
        return systemDiskCategory;
    }

    public void setSystemDiskCategory(String systemDiskCategory) {
        this.systemDiskCategory = systemDiskCategory;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }


    public String getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(String securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    public String getCreateHost() {
        return createHost;
    }

    public void setCreateHost(String createHost) {
        this.createHost = createHost;
    }
}