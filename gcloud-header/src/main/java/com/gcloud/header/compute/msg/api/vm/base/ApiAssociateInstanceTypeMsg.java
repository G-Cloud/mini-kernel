package com.gcloud.header.compute.msg.api.vm.base;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiAssociateInstanceTypeMsg extends ApiMessage {

	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}
	
	@ApiModel(description = "实例类型ID", require = true)
	@NotBlank(message = "0012001::计算规格ID不能为空")
	private String instanceTypeId;
	
//	@ApiModel(description ="可用区ID", require = true)
//	@NotBlank(message = "0012002::可用区ID不能为空")
//	private String zoneId;
	
	@ApiModel(description = "可用区ID列表", require = true)
//	@NotEmpty(message = "0012002::可用区ID不能为空")
	private List<String> zoneIds = new ArrayList<>();

	public String getInstanceTypeId() {
		return instanceTypeId;
	}
	public void setInstanceTypeId(String instanceTypeId) {
		this.instanceTypeId = instanceTypeId;
	}
	public List<String> getZoneIds() {
		return zoneIds;
	}
	public void setZoneIds(List<String> zoneIds) {
		this.zoneIds = zoneIds;
	}
}