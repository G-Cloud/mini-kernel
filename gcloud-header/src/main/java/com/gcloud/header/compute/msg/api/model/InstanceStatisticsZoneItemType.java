package com.gcloud.header.compute.msg.api.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class InstanceStatisticsZoneItemType  implements Serializable {
	@ApiModel(description = "可用区ID")
	private String zoneId;
	@ApiModel(description = "可用区名")
	private String zoneName;
	@ApiModel(description = "统计列表")
	private List<InstanceStatisticsItemType> statisticsItems;
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	public List<InstanceStatisticsItemType> getStatisticsItems() {
		return statisticsItems;
	}
	public void setStatisticsItems(List<InstanceStatisticsItemType> statisticsItems) {
		this.statisticsItems = statisticsItems;
	}
}