/**
 * 域中的用�?
 * @Date 2014-11-20
 * 
 * @Author dengxm
 *
 * @Copywrite 2014 www.g-cloud.com.cn Inc. All rights reserved.
 * 
 * @Description
 */
package com.gcloud.header.identity.ldap;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class LdapUser implements Comparable<LdapUser>,Serializable{
	@ApiModel(description ="邮箱")
	private final String email;
	@ApiModel(description ="用户DN")
    private final String principal;
	@ApiModel(description ="用户Firstname")
    private final String firstname;
	@ApiModel(description ="用户Lastanme")
    private final String lastname;
	@ApiModel(description ="用户�?")
    private final String username;
	@ApiModel(description ="用户别名")
    private final String displayName;
	@ApiModel(description ="域名")
    private final String domain;

    public LdapUser(final String username,final String dispalyName, final String email, final String firstname, final String lastname, final String principal, String domain) {
        this.username = username;
        this.displayName=dispalyName;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.principal = principal;
        this.domain = domain;
    }

    @Override
    public int compareTo(final LdapUser other) {
        return getUsername().compareTo(other.getUsername());
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof LdapUser) {
            final LdapUser otherLdapUser = (LdapUser)other;
            return getUsername().equals(otherLdapUser.getUsername());
        }
        return false;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPrincipal() {
        return principal;
    }
    /**
     * 返回用户�?(登录�?)
     * @return
     */
    public String getUsername() {
        return username;
    }

    public String getDomain() {
        return domain;
    }

    @Override
    public int hashCode() {
        return getUsername().hashCode();
    }

	public String getDisplayName() {
		return displayName;
	}
}