package com.gcloud.header.identity.user;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.common.RegExp;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiUpdateSelfPasswordMsg extends ApiMessage{
	@ApiModel(description = "原密�?", require=true)
	@NotBlank(message="2010701")
	//@Pattern(regexp=RegExp.REGEX_PASSWORD_STRONG, message="2010202::请输�?8-20位的密码，必含字母数字及特殊字符，且以字母开�?")
	private String oldPassword;//登录用密码，�?外部传输，加密不可�??
	
	@ApiModel(description = "新密�?", require=true)
	@NotBlank(message="2010702")
	@Pattern(regexp=RegExp.REGEX_PASSWORD_STRONG, message="2010703::请输�?8-20位的密码，必含字母数字及特殊字符，且以字母开�?")
	private String password;//登录用密码，�?外部传输，加密不可�??
	
	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}