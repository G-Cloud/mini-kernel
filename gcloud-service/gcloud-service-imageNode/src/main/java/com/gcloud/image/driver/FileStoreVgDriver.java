package com.gcloud.image.driver;

import java.io.File;

import org.springframework.stereotype.Component;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.image.enums.ImageResourceType;
import com.gcloud.service.common.lvm.uitls.LvmUtil;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



@Component
public class FileStoreVgDriver implements IImageStoreNodeDriver{

	@Override
	public void downloadImage(String sourceFilePath, String imageId, String target, String imageResourceType) {
		//后续考虑统一放到node节点上执行，也更好统�?的feedback，并且符合长操作统一在node执行的规�?
		//将镜像文件dd到对应的vg�?  dd if=$1 of=/dev/$2/$3 bs=5M
		//�?要创建镜像lv�?  // lvcreate -L 2500 -n volumeName poolName
		String resourcePrefix = imageResourceType.equals(ImageResourceType.IMAGE.value())?"img-":"iso-";
		String resourceName = imageResourceType.equals(ImageResourceType.IMAGE.value())?"镜像":"映像";
		File file = new File(sourceFilePath);
        if (!file.exists()) {
            throw new GCloudException(String.format("::%s文件不存�?", resourceName));
        }
		LvmUtil.lvCreate(target, resourcePrefix + imageId, (int)Math.ceil(file.length()/1024.0/1024.0/1024.0), String.format("创建%s�?%s lv失败", resourceName, imageId));
		LvmUtil.activeLv(LvmUtil.getImagePath(target, imageId));
        LvmUtil.ddToVg(sourceFilePath, target, resourcePrefix + imageId, String.format("::复制%s到vg: %s 失败", resourceName, target));
	}

}