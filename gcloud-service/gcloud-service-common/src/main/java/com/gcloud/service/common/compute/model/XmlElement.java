package com.gcloud.service.common.compute.model;
import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class XmlElement {

	private String name;
	private Map<String, String> attributes;
	private String text;
	private List<XmlElement> childrens;
	
	public XmlElement(String name, String text, Map<String, String> attributes,
			List<XmlElement> childrens) {
		super();
		this.name = name;
		this.text = text;
		this.attributes = attributes;
		this.childrens = childrens;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, String> getAttributes() {
		return attributes;
	}
	public void setAttribute(Map<String, String> attributes) {
		this.attributes = attributes;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public List<XmlElement> getChildrens() {
		return childrens;
	}
	public void setChildrens(List<XmlElement> childrens) {
		this.childrens = childrens;
	}
	
	
}