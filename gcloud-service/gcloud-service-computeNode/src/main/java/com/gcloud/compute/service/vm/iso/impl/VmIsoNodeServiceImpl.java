package com.gcloud.compute.service.vm.iso.impl;

import java.io.File;

import javax.annotation.PostConstruct;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.common.util.StringUtils;
import com.gcloud.compute.prop.ComputeNodeProp;
import com.gcloud.compute.service.vm.iso.IVmIsoNodeService;
import com.gcloud.compute.util.ConfigFileUtil;
import com.gcloud.compute.util.VmNodeUtil;
import com.gcloud.compute.virtual.IVmVirtual;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.simpleflow.Flow;
import com.gcloud.core.simpleflow.SimpleFlowChain;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.enums.PlatformType;
import com.gcloud.header.compute.enums.VmState;
import com.gcloud.header.compute.msg.node.vm.iso.ComputeNodeGetIsoMapMsg;
import com.gcloud.header.compute.msg.node.vm.model.VmCdromDetail;
import com.gcloud.service.common.compute.model.DomainInfo;
import com.gcloud.service.common.compute.uitls.VmUtil;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Slf4j
@Service
public class VmIsoNodeServiceImpl implements IVmIsoNodeService{
	@Autowired
	private IVmVirtual vmVirtual;

	@Autowired
	private ComputeNodeProp nodeProp;
	
	@Autowired
    private MessageBus bus;
	
	@PostConstruct
	private void init() {
	    log.info("[computenode restart init iso rbd map]");
	    try {
	    	String hostname = nodeProp.getHostname();
	        if(StringUtils.isBlank(hostname)){
	            hostname = VmUtil.getHostName();
	        }
	        
		    ComputeNodeGetIsoMapMsg msg =new ComputeNodeGetIsoMapMsg();
		    msg.setServiceId(MessageUtil.controllerServiceId());
	        msg.setHostname(hostname);
	        bus.send(msg);
	    }catch(Exception ex) {
	    	log.error("[computenode init iso rbd map error :]" + ex.getMessage());
	    }
	}

	@Override
	public void configIsoFile(String instanceId,VmCdromDetail vmCdromDetail, String platform) {
		DomainInfo domInfo = VmNodeUtil.checkVm(instanceId);
		if (domInfo == null) {
			throw new GCloudException("::云服务器不存�?");
		}

		String libvirtPath = VmNodeUtil.getLibvirtXmlPath(nodeProp.getNodeIp(), instanceId);
		String instanceXmlPath = VmNodeUtil.getInstanceXmlPath(nodeProp.getNodeIp(), instanceId);
		String isoAttachXmlPath = VmNodeUtil.getIsoXmlPath(nodeProp.getNodeIp(), instanceId, vmCdromDetail.getIsoId());
		
		// 组装VmCdromDetail对象
		VmCdromDetail vmcdromDetail = new VmCdromDetail(vmCdromDetail.getIsoId(), vmCdromDetail.getIsoPath(), vmCdromDetail.getIsoPoolId(), vmCdromDetail.getIsoStorageType(), vmCdromDetail.getIsoTargetDevice());

        SimpleFlowChain<String, String> chain = new SimpleFlowChain<>("config data disk file");

		chain.then(new Flow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                // 修改instance.xml
                ConfigFileUtil.addInstanceXmlCdromInfoAndSave(instanceXmlPath, vmcdromDetail);
                if(StringUtils.isNotBlank(platform) && platform.equals(PlatformType.WINDOWS.getValue())) {
                	ConfigFileUtil.addInstanceXmlFloppyInfoAndSave(instanceXmlPath, nodeProp.getFdaPath());
                }
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
                ConfigFileUtil.deleteElementForXmlAndSave(instanceId, "/instance/cdrom", "id", vmcdromDetail.getIsoId(), instanceXmlPath);
                if(StringUtils.isNotBlank(platform) && platform.equals(PlatformType.WINDOWS.getValue())) {
                	ConfigFileUtil.deleteElementForXmlAndSave(instanceId, "/instance/floppy", "id", "", instanceXmlPath);
                }
                chain.rollback();
            }
        }).then(new Flow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                Document libvirtDoc = ConfigFileUtil.readXml(libvirtPath);
                ConfigFileUtil.deleteLibvirtCdrom(libvirtDoc);
                // 修改libvirt.xml
                attachCdromUpdateLivirtXml(vmcdromDetail, libvirtPath, libvirtDoc);
                if(StringUtils.isNotBlank(platform) && platform.equals(PlatformType.WINDOWS.getValue())) {
                	attachFloppyUpdateLivirtXml(nodeProp.getFdaPath(), libvirtPath, libvirtDoc);
                }
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
                Document libvirtDoc = ConfigFileUtil.readXml(libvirtPath);
                //ConfigFileUtil.deleteLibvirtCdrom(libvirtDoc);
                ConfigFileUtil.updateCDDisk(libvirtDoc, "");
                if(StringUtils.isNotBlank(platform) && platform.equals(PlatformType.WINDOWS.getValue())) {
                	ConfigFileUtil.deleteLibvirtFloppy(libvirtDoc);
                }
                ConfigFileUtil.doc2XmlFile(libvirtDoc, libvirtPath);
                chain.rollback();
            }

        }).then(new Flow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                // 新增iso.xml
            	attachCdromCreateXml(isoAttachXmlPath, vmcdromDetail);
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
                File isoAttachXmlFile = new File(isoAttachXmlPath);
                if(isoAttachXmlFile.exists()){
                	isoAttachXmlFile.delete();
                }
                chain.rollback();

            }
        }).then(new Flow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                if(VmState.RUNNING.value().equals(domInfo.getGcState())){
                    // 挂载iso.xml
                    vmVirtual.attachDevice(instanceId, isoAttachXmlPath);
                }

                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
                if(VmState.RUNNING.value().equals(domInfo.getGcState())){
                    vmVirtual.detachDevice(instanceId, isoAttachXmlPath);
                }
                chain.rollback();

            }
        }).then(new Flow() {
            @Override
            public void run(SimpleFlowChain chain, Object data) {
	                // 更新demain
	            VmNodeUtil.redefineVm(instanceId, libvirtPath);
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, Object data) {
                chain.rollback();
            }
        }).start();

        if (chain.getErrorCode() != null) {
            throw new GCloudException(chain.getErrorCode());
        }
	}
	
	private void attachFloppyUpdateLivirtXml( String fdaPath, String libvirtPath, Document libvirtDoc) {
		ConfigFileUtil.addFloppyDisk(libvirtDoc, fdaPath, "::添加软驱失败");
		ConfigFileUtil.doc2XmlFile(libvirtDoc, libvirtPath);
	}
	
	private void attachCdromUpdateLivirtXml(VmCdromDetail vmcdromDetail, String libvirtPath, Document libvirtDoc) {
		ConfigFileUtil.addCdromDevice(libvirtDoc, vmcdromDetail, false);
		ConfigFileUtil.doc2XmlFile(libvirtDoc, libvirtPath);
	}
	
	private void attachCdromCreateXml(String cdromXmlPath, VmCdromDetail vmcdromDetail) {
		Document volumeDoc = DocumentHelper.createDocument();
		ConfigFileUtil.addCdromDevice(volumeDoc, vmcdromDetail, true);
		ConfigFileUtil.doc2XmlFile(volumeDoc, cdromXmlPath);
	}

	@Override
	public void forceCleanIsoFile(String instanceId) {
		
	}

	@Override
	public void cleanIsoFile(String instanceId, VmCdromDetail vmCdromDetail) {
		DomainInfo domInfo = VmNodeUtil.checkVm(instanceId);
        if (domInfo == null) {
            throw new GCloudException("::云服务器不存�?");
        }

        String state = domInfo.getGcState();
        String isoId = vmCdromDetail.getIsoId();

        String libvirtPath = VmNodeUtil.getLibvirtXmlPath(nodeProp.getNodeIp(), instanceId);
        String instanceXmlPath = VmNodeUtil.getInstanceXmlPath(nodeProp.getNodeIp(), instanceId);
        String isoXmlPath = VmNodeUtil.getIsoXmlPath(nodeProp.getNodeIp(), instanceId, isoId);
        File libvirtFile = new File(libvirtPath);

        SimpleFlowChain<String, String> chain = new SimpleFlowChain<>();
        chain.then(new Flow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                if (VmState.RUNNING.value().equals(state)) {
                	if (VmNodeUtil.isBlkExist(vmVirtual, instanceId, vmCdromDetail.getIsoPath())) {
    					// 置空cdrom
    					// 创建挂载xml文件
                		VmCdromDetail blankCdrom = new VmCdromDetail();
                		blankCdrom.setIsoPath("");
                		blankCdrom.setIsoId(vmCdromDetail.getIsoId());
                		blankCdrom.setIsoTargetDevice(vmCdromDetail.getIsoTargetDevice());
                		blankCdrom.setIsoStorageType(vmCdromDetail.getIsoStorageType());
                		attachCdromCreateXml(isoXmlPath, blankCdrom);

    					VmNodeUtil.attachTimes(instanceId, isoXmlPath, 3);
    				}
                }
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
            	attachCdromCreateXml(isoXmlPath, vmCdromDetail);
                if (VmState.RUNNING.value().equals(state)) {
                    VmNodeUtil.attachTimes(instanceId, isoXmlPath, 3);
                }
                chain.rollback();
            }
        }).then(new Flow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                ConfigFileUtil.deleteElementForXmlAndSave(instanceId, "/instance/cdrom", "id", String.valueOf(isoId), instanceXmlPath);
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
                ConfigFileUtil.addInstanceXmlCdromInfoAndSave(instanceXmlPath, vmCdromDetail);
                chain.rollback();
            }
        }).then(new Flow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                Document libvirtDoc = ConfigFileUtil.readXml(libvirtFile);
                //ConfigFileUtil.deleteLibvirtCdrom(libvirtDoc);
                ConfigFileUtil.updateCDDisk(libvirtDoc, "");
                ConfigFileUtil.doc2XmlFile(libvirtDoc, libvirtPath);
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
                Document libvirtDoc = ConfigFileUtil.readXml(libvirtFile);
                /*ConfigFileUtil.deleteLibvirtCdrom(libvirtDoc);
                attachCdromUpdateLivirtXml(vmCdromDetail, libvirtPath, libvirtDoc);*/
                ConfigFileUtil.updateCDDisk(libvirtDoc, vmCdromDetail.getIsoPath());
                ConfigFileUtil.doc2XmlFile(libvirtDoc, libvirtPath);
                chain.rollback();
            }
        }).then(new Flow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                File aFile = new File(isoXmlPath);
                if (aFile.exists()) {
                    aFile.delete();
                }
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
                File aFile = new File(isoXmlPath);
                if (!aFile.exists()) {
                    attachCdromCreateXml(isoXmlPath, vmCdromDetail);
                }
                chain.rollback();
            }
        }).then(new Flow() {
            @Override
            public void run(SimpleFlowChain chain, Object data) {
                // 更新demain
                VmNodeUtil.redefineVm(instanceId, libvirtPath);
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, Object data) {
                chain.rollback();
            }
        }).start();

        if (chain.getErrorCode() != null) {
            throw new GCloudException(chain.getErrorCode());
        }
	}

}