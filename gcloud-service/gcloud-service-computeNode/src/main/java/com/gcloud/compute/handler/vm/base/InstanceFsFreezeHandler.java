package com.gcloud.compute.handler.vm.base;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.common.util.StringUtils;
import com.gcloud.compute.service.vm.base.IVmBaseNodeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.senior.InstanceFsFreezeMsg;
import com.gcloud.header.compute.msg.node.vm.senior.InstanceFsFreezeReplyMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Slf4j
@Handler
public class InstanceFsFreezeHandler extends AsyncMessageHandler<InstanceFsFreezeMsg>{
	@Autowired
    private IVmBaseNodeService vmBaseNodeService;
	
	@Autowired
    private MessageBus bus;
	
	@Override
	public void handle(InstanceFsFreezeMsg msg) throws GCloudException {
		InstanceFsFreezeReplyMsg replyMsg = msg.deriveMsg(InstanceFsFreezeReplyMsg.class);
        replyMsg.setSuccess(false);
        replyMsg.setServiceId(MessageUtil.controllerServiceId());
        try{
        	boolean result = vmBaseNodeService.fsFreeze(msg.getInstanceId(), msg.getFsfreezeType());
            replyMsg.setSuccess(result);
        }catch (Exception ex){
            log.error("::冻结/解冻虚拟机文件系统失�?", ex);
            replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(ex, "::冻结/解冻虚拟机文件系统失�?"));
        }
        if(!replyMsg.getSuccess() && StringUtils.isBlank(replyMsg.getErrorCode())) {
        	replyMsg.setErrorCode("::冻结/解冻虚拟机文件系统失�?");
        }
        bus.send(replyMsg);
	}

}