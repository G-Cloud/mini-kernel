package com.gcloud.compute.handler.vm.trash;

import com.gcloud.compute.service.vm.trash.IVmTrashNodeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.trash.CleanInstanceInfoMsg;
import com.gcloud.header.compute.msg.node.vm.trash.CleanInstanceInfoReplyMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
@Handler
public class CleanInstanceInfoHandler extends AsyncMessageHandler<CleanInstanceInfoMsg> {

    @Autowired
    private IVmTrashNodeService vmTrashNodeService;

    @Autowired
    private MessageBus bus;

    @Override
    public void handle(CleanInstanceInfoMsg msg) {

        CleanInstanceInfoReplyMsg replyMsg = msg.deriveMsg(CleanInstanceInfoReplyMsg.class);
        replyMsg.setSuccess(false);
        replyMsg.setServiceId(MessageUtil.controllerServiceId());
        try{
            vmTrashNodeService.cleanInstanceInfo(msg.getInstanceId());
            replyMsg.setSuccess(true);
        }catch (Exception ex){
            log.error("清理虚拟机信息失�?", ex);
            replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(ex, "::清理虚拟机信息失�?"));
        }
        bus.send(replyMsg);

    }
}