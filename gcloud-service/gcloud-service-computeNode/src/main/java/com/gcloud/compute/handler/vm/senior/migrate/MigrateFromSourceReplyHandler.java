package com.gcloud.compute.handler.vm.senior.migrate;

import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateFromSourceReplyMsg;
import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Handler
public class MigrateFromSourceReplyHandler extends AsyncMessageHandler<MigrateFromSourceReplyMsg>{

	@Override
	public void handle(MigrateFromSourceReplyMsg msg) {
		log.debug("MigrateFromSourceReplyMsg succ");
	}

}