#!/bin/bash
container_name=gcloud_node
start_network_script=/usr/share/gcloud/network/start-network.sh
times=10
interval=10

function monitor {
  dockerRunningState=$(docker inspect -f '{{.State.Running}}' $container_name)
  if [ $dockerRunningState ]
  then
    docker exec $container_name $start_network_script
    return 1
  else
    return 0
  fi
}

int=1
while(( $int<=$times ))
do
    monitor
    if [ $? -eq 1  ]
    then
      break
    fi
    let "int++"
    sleep $interval
done
