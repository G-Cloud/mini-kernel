package com.gcloud.api;

import com.gcloud.api.error.ApiErrorCodes;
import com.gcloud.api.log.LogComponent;
import com.gcloud.common.constants.HttpRequestConstant;
import com.gcloud.common.util.StringUtils;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.handle.MessageHandlerKeeper;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.service.ServiceModuleMapping;
import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;
import java.util.UUID;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@RestController
@RequestMapping("/")
@ResponseBody
@Slf4j
public class StandardApiController {

	private static Set<String> localModules = Sets.newHashSet(Module.REGION.toString());
	private static Set<String> localUrl = Sets.newHashSet("/ecs/DescribeRegions.do");
	
	//@Value("${gcloud.service.controller}")
	//private String serviceId;
	
	@Autowired
	ServiceModuleMapping serviceModuleMapping;
	@Autowired
	MessageBus bus;
	
	@Autowired
	LogComponent logComponent;

	@RequestMapping(value = "/{module}/{action}.do")
	public ResponseEntity api(@PathVariable String module, @PathVariable String action, @Validated ApiMessage message, HttpServletRequest request) throws GCloudException{

		if(message!=null) {
			if (StringUtils.isBlank(message.getTaskId())) {
				message.setTaskId(UUID.randomUUID().toString());
	        }

			String serviceId = serviceModuleMapping.getService(module.toUpperCase());

			ApiReplyMessage replyMsg = null;
			if(localModules.contains(module.toUpperCase()) || localModules.contains(request.getRequestURI())) {
				log.debug("ApiController api ,module=" + module + ",action=" + action + ", currentThread:" + Thread.currentThread().getId());
				MessageHandler handler = null;
			    handler = MessageHandlerKeeper.get(message.getClass().getName());
				GCloudException gex = null;
			    try {
			    	replyMsg = (ApiReplyMessage)handler.handle(message);
			    } catch(GCloudException ge) {
					gex = ge;
			    	throw ge;
			    } catch(Exception e) {
			    	gex = new GCloudException("api-local-handler-0001::系统异常，请联系管理�?");
			    	throw gex;
			    }finally {
					logComponent.logRecord(serviceId, message, handler, gex);
				}
			    
			} else {
				message.setServiceId(serviceId);
				replyMsg = (ApiReplyMessage) bus.call(message);
			}

			if(replyMsg == null){
				throw new GCloudException("::操作超时");
			}else{
				if(replyMsg.getSuccess() != null && !replyMsg.getSuccess()){
					throw new GCloudException(replyMsg.getErrorMsg(), replyMsg.getErrorParam());
				}else if(StringUtils.isBlank(replyMsg.getRequestId())){
					String requestId = ObjectUtils.toString(request.getAttribute(HttpRequestConstant.ATTR_REQUEST_ID), null);
					replyMsg.setRequestId(requestId);
				}
				return new ResponseEntity(replyMsg, HttpStatus.OK);
			}

		}else{
			throw new GCloudException(ApiErrorCodes.API_NOT_FOUND + "::api not found");
		}
	}
}