package com.gcloud.controller.image.handler.api.iso;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.model.iso.CreateIsoParams;
import com.gcloud.controller.image.service.IIsoService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.image.msg.api.iso.ApiImportIsoMsg;
import com.gcloud.header.image.msg.api.iso.ApiImportIsoReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



@LongTask
@GcLog(taskExpect = "上传映像")
@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="ImportIso",name="导入映像")
public class ApiImportIsoHandler extends MessageHandler<ApiImportIsoMsg, ApiImportIsoReplyMsg> {
	@Autowired
    private IIsoService isoService;
	
	@Override
	public ApiImportIsoReplyMsg handle(ApiImportIsoMsg msg) throws GCloudException {
		CreateIsoParams params = BeanUtil.copyProperties(msg, CreateIsoParams.class);
		String isoId = isoService.createIso(params, msg.getCurrentUser());
        ApiImportIsoReplyMsg reply = new ApiImportIsoReplyMsg();
        reply.setIsoId(isoId);
        msg.setObjectId(isoId);
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.ISO_NAME, isoId));
        return reply;
	}

}