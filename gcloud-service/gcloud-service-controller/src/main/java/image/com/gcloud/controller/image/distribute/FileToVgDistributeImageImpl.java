package com.gcloud.controller.image.distribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.controller.image.driver.ImageDriverEnum;
import com.gcloud.controller.image.enums.ImageDistributeTargetType;
import com.gcloud.controller.image.prop.ImageProp;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.service.ServiceName;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.image.enums.ImageResourceType;
import com.gcloud.header.image.msg.node.DownloadImageMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
public class FileToVgDistributeImageImpl implements IDistributeImage {
	@Autowired
	ImageProp prop;
	
	@Autowired
	private MessageBus bus;
	
	@Autowired
	ServiceName serviceName;

	@Override
	public void distributeImage(String imageId, String target, String taskId, String resourceType, String distributeAction) {
		DownloadImageMsg downloadMsg = new DownloadImageMsg();
        downloadMsg.setServiceId(serviceName.getImageSchedule());
        downloadMsg.setImageId(imageId);
        downloadMsg.setImagePath((resourceType.equals(ImageResourceType.IMAGE.value())?prop.getImageFilesystemStoreDir():prop.getIsoFilesystemStoreDir()) + imageId);
        downloadMsg.setProvider(ProviderType.GCLOUD.name().toLowerCase());
        downloadMsg.setImageStroageType(ImageDriverEnum.FILE.getStorageType());
        downloadMsg.setTarget(target);
        downloadMsg.setTargetType(ImageDistributeTargetType.VG.value());
        downloadMsg.setTaskId(taskId);
        downloadMsg.setImageResourceType(resourceType);
        
        bus.send(downloadMsg);
		//后续考虑统一放到node节点上执行，也更好统�?的feedback，并且符合长操作统一在node执行的规�?
		//将镜像文件dd到对应的vg�?  dd if=$1 of=/dev/$2/$3 bs=5M
		//�?要创建镜像lv�?  // lvcreate -L 2500 -n volumeName poolName
		/*Image img = imageDao.getById(imageId);
		LvmUtil.lvCreate(target, "img-" + imageId, (int)Math.ceil(img.getSize()/1024.0/1024.0/1024.0), "创建镜像�?" + imageId + "lv失败");
		LvmUtil.activeLv(LvmUtil.getImagePath(target, imageId));
        LvmUtil.ddToVg(prop.getImageFilesystemStoreDir() + imageId, target, "img-" + imageId, "::复制镜像到vg:" + target + "失败");
		//feedback task
        if(StringUtils.isNotBlank(taskId)) {
        	WorkFlowEngine.feedbackHandler(taskId, FeedbackState.SUCCESS.name(), "");
        }*/
	}
	
	/*@Override
	public void deleteImageCache(String imageId, String target, String taskId) {
		//删掉vg上对应的image cache
	}*/

}