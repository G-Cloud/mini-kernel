package com.gcloud.controller.image.handler.api.iso;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.model.iso.DetailIsoParams;
import com.gcloud.controller.image.service.IIsoService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.image.model.iso.DetailIso;
import com.gcloud.header.image.msg.api.iso.ApiDetailIsoMsg;
import com.gcloud.header.image.msg.api.iso.ApiDetailIsoReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="DetailIso",name="映像详情")
public class ApiDetailIsoHandler extends MessageHandler<ApiDetailIsoMsg, ApiDetailIsoReplyMsg>{
	@Autowired
	private IIsoService isoService;
	
	@Override
	public ApiDetailIsoReplyMsg handle(ApiDetailIsoMsg msg) throws GCloudException {
		DetailIsoParams params = BeanUtil.copyProperties(msg, DetailIsoParams.class);
		DetailIso response = isoService.detailIso(params, msg.getCurrentUser());
		
		ApiDetailIsoReplyMsg repley = new ApiDetailIsoReplyMsg();
		repley.setDetailIso(response);
		return repley;
	}

}