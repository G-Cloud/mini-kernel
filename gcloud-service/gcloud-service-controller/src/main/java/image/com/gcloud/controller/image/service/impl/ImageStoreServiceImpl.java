package com.gcloud.controller.image.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.controller.image.dao.ImageStoreDao;
import com.gcloud.controller.image.entity.ImageStore;
import com.gcloud.controller.image.service.IImageStoreService;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Service
public class ImageStoreServiceImpl implements IImageStoreService {
	@Autowired
	ImageStoreDao storeDao;

	@Override
	public void update(ImageStore store, List<String> fields) {
		storeDao.update(store, fields);
	}
	
	@Override
	public ImageStore findUniqueByProperties(Map<String, Object> props) {
		return storeDao.findUniqueByProperties(props);
	}
	
	@Override
	public void delete(ImageStore store) {
		storeDao.delete(store);
	}

	@Override
	public ImageStore getById(String id) {
		return storeDao.getById(id);
	}
}