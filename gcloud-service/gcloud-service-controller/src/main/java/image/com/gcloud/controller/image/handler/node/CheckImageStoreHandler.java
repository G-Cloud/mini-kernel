package com.gcloud.controller.image.handler.node;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.entity.ImageStore;
import com.gcloud.controller.image.service.IImageStoreService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.image.enums.ImageResourceType;
import com.gcloud.header.image.msg.node.CheckImageStoreMsg;
import com.gcloud.header.image.msg.node.CheckImageStoreReplyMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Handler
@Slf4j
public class CheckImageStoreHandler extends MessageHandler<CheckImageStoreMsg, CheckImageStoreReplyMsg>{
	@Autowired
	IImageStoreService imageStoreService;
	
	@Override
	public CheckImageStoreReplyMsg handle(CheckImageStoreMsg msg) throws GCloudException {
		log.info("CheckImageStoreHandler start");
		// 修改image_store_info表对应记录状�?
		ImageStore	store = imageStoreService.getById(String.format("%s_%s_%s_%s", msg.getImageId(), msg.getTargetType(), msg.getTarget(), msg.getImageResourceType()));
		CheckImageStoreReplyMsg reply = new CheckImageStoreReplyMsg();
		reply.setStatus(store!=null?store.getStatus():"");
		
		return reply;
	}

}