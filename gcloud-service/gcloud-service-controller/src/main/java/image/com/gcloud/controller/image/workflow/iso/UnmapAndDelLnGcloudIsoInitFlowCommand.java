package com.gcloud.controller.image.workflow.iso;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.image.dao.IsoMapInfoDao;
import com.gcloud.controller.image.entity.IsoMapInfo;
import com.gcloud.controller.image.workflow.model.iso.UnmapAndDelLnGcloudIsoInitFlowCommandReq;
import com.gcloud.controller.image.workflow.model.iso.UnmapAndDelLnGcloudIsoInitFlowCommandRes;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class UnmapAndDelLnGcloudIsoInitFlowCommand extends BaseWorkFlowCommand{
	@Autowired
	IsoMapInfoDao isoMapDao;
	
	@Override
	protected Object process() throws Exception {
		UnmapAndDelLnGcloudIsoInitFlowCommandReq req = (UnmapAndDelLnGcloudIsoInitFlowCommandReq)getReqParams();
		List<IsoMapInfo> isoMaps = isoMapDao.findByProperty("isoId", req.getIsoId());
		UnmapAndDelLnGcloudIsoInitFlowCommandRes res = new UnmapAndDelLnGcloudIsoInitFlowCommandRes();
		res.setIsoMaps(isoMaps);
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return UnmapAndDelLnGcloudIsoInitFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return UnmapAndDelLnGcloudIsoInitFlowCommandRes.class;
	}

}