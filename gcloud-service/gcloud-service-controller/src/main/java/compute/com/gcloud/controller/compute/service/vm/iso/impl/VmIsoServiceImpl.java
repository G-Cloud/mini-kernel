package com.gcloud.controller.compute.service.vm.iso.impl;

import com.gcloud.controller.compute.ControllerComputeProp;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.model.node.Node;
import com.gcloud.controller.compute.service.vm.iso.IVmIsoService;
import com.gcloud.controller.compute.utils.RedisNodesUtil;
import com.gcloud.controller.image.dao.IsoDao;
import com.gcloud.controller.image.dao.VmIsoAttachmentDao;
import com.gcloud.controller.image.entity.Iso;
import com.gcloud.controller.image.entity.VmIsoAttachment;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.compute.enums.VmTaskState;
import com.gcloud.header.compute.msg.node.vm.model.VmCdromDetail;
import com.gcloud.service.common.compute.uitls.VmUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Service
public class VmIsoServiceImpl implements IVmIsoService {
	@Autowired
	IsoDao isoDao;
	
	@Autowired
	private InstanceDao instanceDao;
	
	@Autowired
    private VmIsoAttachmentDao isoAttachmentDao;

	@Autowired
	private ControllerComputeProp prop;
	
	public void attachIsoInit(String instanceId, String isoId, boolean inTask) {
		VmInstance vm = instanceDao.getById(instanceId);
		if (vm == null) {
			throw new GCloudException("0011403::云服务器不存�?");
		}

		Iso iso = isoDao.getById(isoId);
		if (iso == null) {
			throw new GCloudException("0011404::映像不存�?");
		}

		Node node = RedisNodesUtil.getComputeNodeByHostName(vm.getHostname());
		if (node == null) {
			throw new GCloudException("0011405::找不到云服务器所在节�?");
		}

		if (!inTask) {
			if (!instanceDao.updateInstanceTaskState(instanceId, VmTaskState.ATTACH_ISO)) {
				throw new GCloudException("0011406::云服务器当前无法挂载光盘");
			}
		}
		
	}

	@Override
	public void detachIsoApiCheck(String isoId) {
		Iso iso = isoDao.getById(isoId);
		if (iso == null) {
			throw new GCloudException("0011504::映像不存�?");
		}
	}
	
	@Override
	public void detachIsoInit(String instanceId, String isoId, boolean inTask) {

		VmInstance vm = instanceDao.getById(instanceId);
		if (vm == null) {
			throw new GCloudException("0011503::云服务器不存�?");
		}

		detachIsoApiCheck(isoId);

		List<VmIsoAttachment> isoAttachments = isoAttachmentDao.findByIsoIdAndInstanceId(isoId, instanceId);
        if (isoAttachments == null || isoAttachments.size() == 0) {
            throw new GCloudException("0011505::云服务器没有挂载此映�?");
        }

		Node node = RedisNodesUtil.getComputeNodeByHostName(vm.getHostname());
		if (node == null) {
			throw new GCloudException("0011506::找不到云服务器所在节�?");
		}

		if (!inTask) {
			if (!instanceDao.updateInstanceTaskState(instanceId, VmTaskState.DETACH_ISO)) {
				throw new GCloudException("0011507::云服务器当前状�?�不能卸载云�?");
			}
		}

	}
	
	@Override
	public VmCdromDetail isoDetail(String isoId, String instanceId) {
		VmCdromDetail response = new VmCdromDetail();
		Iso iso = isoDao.getById(isoId);
        if (iso == null) {
            throw new GCloudException("::找不到对应的映像");
        }
        List<VmIsoAttachment> isoAttachments = isoAttachmentDao.findByIsoIdAndInstanceId(isoId, instanceId);
        if (isoAttachments == null || isoAttachments.size() == 0) {
            throw new GCloudException("::云服务器没有挂载此映�?");
        }
        VmIsoAttachment attach = isoAttachments.get(0);
        response.setIsoId(isoId);

//        response.setIsoPath(VmUtil.getIsoPath(isoId, attach.getIsoPool(), StorageType.value(attach.getIsoStorageType()).getDefaultDriver().name()));
		String isoPath = VmUtil.getIsoPath(iso.getId(), attach.getIsoStorageType().equals(StorageType.DISTRIBUTED.getValue())?prop.getCephIsoPool():(attach.getIsoStorageType().equals(StorageType.LOCAL.getValue())?prop.getIsoCachedPath():attach.getIsoPool()), StorageType.value(attach.getIsoStorageType()).getDefaultDriver().name());
		response.setIsoPath(isoPath);

        response.setIsoPoolId(attach.getIsoPoolId());
        response.setIsoPool(attach.getIsoPool());
        response.setIsoStorageType(attach.getIsoStorageType());
        response.setIsoTargetDevice(attach.getMountpoint());
		return response;
	}

}