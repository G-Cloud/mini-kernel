package com.gcloud.controller.compute.workflow.model.senior.migrate;

import com.gcloud.controller.compute.workflow.model.senior.migrate.model.MigrateDiskInfo;
import com.gcloud.controller.compute.workflow.model.senior.migrate.model.MigrateIsoInfo;
import com.gcloud.controller.compute.workflow.model.senior.migrate.model.MigrateNetcardInfo;
import com.gcloud.core.workflow.model.WorkflowFirstStepResException;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class MigrateVmInitFlowCommandRes extends WorkflowFirstStepResException{
	private String taskId;
	private String instanceId;
	private String sourceHostName;
	private String targetHostName;
	private String beginStatus;
	private List<MigrateIsoInfo> isos;
	private List<MigrateDiskInfo> disks;
	private List<MigrateNetcardInfo> netcards;
	private String imageId;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getSourceHostName() {
		return sourceHostName;
	}

	public void setSourceHostName(String sourceHostName) {
		this.sourceHostName = sourceHostName;
	}

	public String getBeginStatus() {
		return beginStatus;
	}

	public void setBeginStatus(String beginStatus) {
		this.beginStatus = beginStatus;
	}

	public List<MigrateIsoInfo> getIsos() {
		return isos;
	}

	public void setIsos(List<MigrateIsoInfo> isos) {
		this.isos = isos;
	}

	public List<MigrateDiskInfo> getDisks() {
		return disks;
	}

	public void setDisks(List<MigrateDiskInfo> disks) {
		this.disks = disks;
	}

	public List<MigrateNetcardInfo> getNetcards() {
		return netcards;
	}

	public void setNetcards(List<MigrateNetcardInfo> netcards) {
		this.netcards = netcards;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getTargetHostName() {
		return targetHostName;
	}

	public void setTargetHostName(String targetHostName) {
		this.targetHostName = targetHostName;
	}
}