package com.gcloud.controller.compute.handler.api.vm.storage.standard;

import java.util.UUID;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.workflow.model.storage.AttachDataDiskInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.model.storage.AttachDataDiskWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.storage.AttachDataDiskWorkflow;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.storage.standard.StandardApiAttachDiskMsg;
import com.gcloud.header.compute.msg.api.vm.storage.standard.StandardApiAttachDiskReplyMsg;
import com.gcloud.header.log.model.Task;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@LongTask
@GcLog(isMultiLog = true, taskExpect = "挂载磁盘")
@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = "AttachDisk", versions = {ApiVersion.Standard})
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.VOLUME, resourceIdField = "diskId")
public class StandardApiAttachDiskHandler extends BaseWorkFlowHandler<StandardApiAttachDiskMsg, StandardApiAttachDiskReplyMsg>{

	@Override
	public Object preProcess(StandardApiAttachDiskMsg msg) throws GCloudException {
		return null;
	}

	@Override
	public StandardApiAttachDiskReplyMsg process(StandardApiAttachDiskMsg msg) throws GCloudException {
		StandardApiAttachDiskReplyMsg reply = new StandardApiAttachDiskReplyMsg();
        // 记录操作日志，任务流无论单操作还是批�? 都用这种方式记录操作日志
        AttachDataDiskInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), AttachDataDiskInitFlowCommandRes.class);
        String except = String.format("挂载磁盘[%s]成功", res.getVolumeId());
        reply.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(res.getVolumeId()).objectName(res.getVolumeName()).expect(except).build());

        String requestId = UUID.randomUUID().toString();
        reply.setRequestId(requestId);
        reply.setSuccess(true);
        return reply;
	}

	@Override
	public Class getWorkflowClass() {
		return AttachDataDiskWorkflow.class;
	}
	
	@Override
    public Object initParams(StandardApiAttachDiskMsg msg) {
        AttachDataDiskWorkflowReq req = new AttachDataDiskWorkflowReq();
        req.setInstanceId(msg.getInstanceId());
        req.setVolumeId(msg.getDiskId());
        req.setInTask(false);
        return req;
    }

}