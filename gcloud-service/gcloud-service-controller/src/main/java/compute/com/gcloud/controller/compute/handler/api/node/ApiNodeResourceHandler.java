package com.gcloud.controller.compute.handler.api.node;

import com.gcloud.controller.compute.service.node.IComputeNodeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.node.ApiNodeResourceMsg;
import com.gcloud.header.compute.msg.api.node.ApiNodeResourceReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module = Module.ECS,subModule= SubModule.VM,action = "NodeResource", name = "节点资源")
public class ApiNodeResourceHandler extends MessageHandler<ApiNodeResourceMsg, ApiNodeResourceReplyMsg> {

    @Autowired
    private IComputeNodeService nodeService;

    @Override
    public ApiNodeResourceReplyMsg handle(ApiNodeResourceMsg msg) throws GCloudException {
        ApiNodeResourceReplyMsg reply = new ApiNodeResourceReplyMsg();
        reply.setNodeResource(nodeService.nodeResource());
        return reply;
    }
}