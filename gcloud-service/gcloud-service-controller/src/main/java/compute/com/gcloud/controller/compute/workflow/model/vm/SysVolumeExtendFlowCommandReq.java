package com.gcloud.controller.compute.workflow.model.vm;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class SysVolumeExtendFlowCommandReq {
	private String instanceUuid;
	private String volumeId;
	private int imageSize;
	private int systemDiskSize;//传入的大�?
	private int systemQcow2Size;
	
	public String getInstanceUuid() {
		return instanceUuid;
	}
	public void setInstanceUuid(String instanceUuid) {
		this.instanceUuid = instanceUuid;
	}
	public String getVolumeId() {
		return volumeId;
	}
	public void setVolumeId(String volumeId) {
		this.volumeId = volumeId;
	}
	public int getImageSize() {
		return imageSize;
	}
	public void setImageSize(int imageSize) {
		this.imageSize = imageSize;
	}
	public int getSystemDiskSize() {
		return systemDiskSize;
	}
	public void setSystemDiskSize(int systemDiskSize) {
		this.systemDiskSize = systemDiskSize;
	}
	public int getSystemQcow2Size() {
		return systemQcow2Size;
	}
	public void setSystemQcow2Size(int systemQcow2Size) {
		this.systemQcow2Size = systemQcow2Size;
	}
}