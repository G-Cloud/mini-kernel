package com.gcloud.controller.compute.workflow.vm.create;


import com.gcloud.controller.compute.service.vm.create.IVmCreateService;
import com.gcloud.controller.compute.workflow.model.vm.CreateInstanceWorkflowReq;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
public class CreateInstanceWorkflow extends BaseWorkFlows {

	@Autowired
	private IVmCreateService vmCreateService;

	@Override
	public String getFlowTypeCode() {
		return "CreateInstanceWorkflow";
	}

	@Override
	public void process() {

	}

	@Override
    protected Class<?> getReqParamClass() {
        return CreateInstanceWorkflowReq.class;
    }

	@Override
	public Object preProcess() {
		return null;
	}
}