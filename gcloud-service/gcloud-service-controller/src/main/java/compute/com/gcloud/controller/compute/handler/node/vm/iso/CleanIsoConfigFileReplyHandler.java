package com.gcloud.controller.compute.handler.node.vm.iso;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.dao.VmIsoAttachmentDao;
import com.gcloud.controller.image.entity.VmIsoAttachment;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.compute.enums.VmIsoAttachStatus;
import com.gcloud.header.compute.msg.node.vm.iso.CleanIsoConfigFileReplyMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Handler
@Slf4j
public class CleanIsoConfigFileReplyHandler  extends AsyncMessageHandler<CleanIsoConfigFileReplyMsg>{
	@Autowired
    private VmIsoAttachmentDao isoAttachmentDao;

	@Override
	public void handle(CleanIsoConfigFileReplyMsg msg) {
		log.debug("CleanIsoConfigFileReplyHandler" + msg.getSuccess());
		List<VmIsoAttachment> isoAttachments = isoAttachmentDao.findByIsoIdAndInstanceId(msg.getIsoId(), msg.getInstanceId());
        VmIsoAttachment attach = isoAttachments.get(0);
        
		if(msg.getSuccess()) {
	        isoAttachmentDao.delete(attach);
		} else {
			attach.setStatus(VmIsoAttachStatus.ATTACHED.name());
			isoAttachmentDao.update(attach);
		}
	}

}