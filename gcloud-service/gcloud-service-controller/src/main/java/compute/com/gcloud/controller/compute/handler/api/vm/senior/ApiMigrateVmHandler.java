package com.gcloud.controller.compute.handler.api.vm.senior;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.workflow.model.senior.migrate.MigrateVmInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.model.senior.migrate.MigrateVmWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.senior.migrate.MigrateVmWorkflow;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.senior.ApiMigrateVmMsg;
import com.gcloud.header.compute.msg.api.vm.senior.ApiMigrateVmReplyMsg;
import com.gcloud.header.log.model.Task;
import org.springframework.beans.factory.annotation.Value;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule=SubModule.VM, action = "MigrateVm", name = "云服务器迁移")
@LongTask
@GcLog(isMultiLog = true, taskExpect = "云服务器迁移")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
public class ApiMigrateVmHandler extends BaseWorkFlowHandler<ApiMigrateVmMsg, ApiMigrateVmReplyMsg>{

	@Value("${spring.computeNode.vmMigrateType:TCP}")
	private String type;
	
	@Override
	public Object preProcess(ApiMigrateVmMsg msg) throws GCloudException {
		return null;
	}

	@Override
	public ApiMigrateVmReplyMsg process(ApiMigrateVmMsg msg) throws GCloudException {
		ApiMigrateVmReplyMsg reply = new ApiMigrateVmReplyMsg();
		MigrateVmInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), MigrateVmInitFlowCommandRes.class);
		reply.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getInstanceId()).objectName(CacheContainer.getInstance().getString(CacheType.INSTANCE_ALIAS, msg.getInstanceId())).expect("迁移云服务器成功").build());
		reply.setInstanceId(res.getInstanceId());
		return reply;
	}

	@Override
	public Class getWorkflowClass() {
		return MigrateVmWorkflow.class;
	}

	@Override
	public Object initParams(ApiMigrateVmMsg msg) {
		MigrateVmWorkflowReq req = new MigrateVmWorkflowReq();
		req.setInstanceId(msg.getInstanceId());
		req.setTargetHostName(msg.getTargetHostName());
		req.setPoolId(msg.getPoolId());
		req.setStorageType(msg.getStorageType());
		req.setCurrentUser(msg.getCurrentUser());
		return req;
	}
}