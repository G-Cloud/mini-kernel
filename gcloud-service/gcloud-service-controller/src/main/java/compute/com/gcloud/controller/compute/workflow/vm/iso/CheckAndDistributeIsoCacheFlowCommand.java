package com.gcloud.controller.compute.workflow.vm.iso;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.compute.ControllerComputeProp;
import com.gcloud.controller.compute.workflow.model.iso.CheckAndDistributeIsoCacheFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.iso.CheckAndDistributeIsoCacheFlowCommandRes;
import com.gcloud.controller.image.entity.ImageStore;
import com.gcloud.controller.image.enums.ImageDistributeTargetType;
import com.gcloud.controller.image.enums.ImageStorageType;
import com.gcloud.controller.image.model.iso.DistributeIsoParams;
import com.gcloud.controller.image.prop.ImageProp;
import com.gcloud.controller.image.provider.IIsoProvider;
import com.gcloud.controller.image.service.IImageStoreService;
import com.gcloud.controller.storage.dao.StoragePoolDao;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.image.enums.ImageResourceType;
import com.gcloud.service.common.enums.ImageStoreStatus;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
@Slf4j
public class CheckAndDistributeIsoCacheFlowCommand extends BaseWorkFlowCommand{
	@Autowired
	IImageStoreService imageStoreService;
	
	@Autowired
	ImageProp imageProp;
	
	@Autowired
    private ControllerComputeProp prop;
	
	@Autowired
    private StoragePoolDao storagePoolDao;
	
	@Override
	public boolean judgeExecute() {
		CheckAndDistributeIsoCacheFlowCommandReq req = (CheckAndDistributeIsoCacheFlowCommandReq)getReqParams();
		
		StoragePool pool = this.storagePoolDao.getById(req.getStoragePoolId());
		
		//�?要skip的情�?1、Glance 非local虚拟机；2、Gcloud rbd后端 rbd虚拟机；3、除上面两种情况外，store不为空；
		if(ResourceProviders.getDefault(ResourceType.IMAGE).providerType().equals(ProviderType.GLANCE)
			&& !pool.getStorageType().equals(StorageType.LOCAL.name().toLowerCase())) {
			return false;
		}
		if(ResourceProviders.getDefault(ResourceType.IMAGE).providerType().equals(ProviderType.GCLOUD) 
		&& imageProp.getStroageType().equals(ImageStorageType.RBD.value())
		&& pool.getStorageType().equals(StorageType.DISTRIBUTED.name().toLowerCase())) {
			return false;
		}
		
		Map<String, Object> props = new HashMap<String, Object>();
		if(pool.getStorageType().equals(StorageType.LOCAL.name().toLowerCase())) {
			props.put("storeTarget", req.getHostname());
		} else if(pool.getStorageType().equals(StorageType.DISTRIBUTED.name().toLowerCase())) {
			props.put("storeTarget", prop.getCephIsoPool());
		} else {
			props.put("storeTarget", pool.getPoolName());
		}
		
		
		props.put("imageId", req.getIsoId());
		props.put("resourceType", ImageResourceType.ISO.value());
		
		ImageStore store = imageStoreService.findUniqueByProperties(props);
		/*if(store != null && !store.getStatus().equals(ImageStoreStatus.ACTIVE.value())) {
			throw new GCloudException("::映像在分发中，请等待分发完成后再尝试操作");
		}*/
		
		return store == null || (store != null && !store.getStatus().equals(ImageStoreStatus.ACTIVE.value()));
	}
	
	@Override
	protected Object process() throws Exception {
		CheckAndDistributeIsoCacheFlowCommandReq req = (CheckAndDistributeIsoCacheFlowCommandReq)getReqParams();
		
		StoragePool pool = this.storagePoolDao.getById(req.getStoragePoolId());
		String targetType = "";
		String target = "";
		if(pool.getStorageType().equals(StorageType.LOCAL.name().toLowerCase())) {
			targetType = ImageDistributeTargetType.NODE.value();
			target = req.getHostname();
		}else if(pool.getStorageType().equals(StorageType.DISTRIBUTED.name().toLowerCase())) {
			targetType = ImageDistributeTargetType.RBD.value();
			target = prop.getCephIsoPool();//rbd的映像缓存需要放至images�?
		} else {
			targetType = ImageDistributeTargetType.VG.value();
			target = pool.getPoolName();
		}
		
		DistributeIsoParams params = new DistributeIsoParams();
		params.setIsoId(req.getIsoId());
		params.setTarget(target);
		params.setTargetType(targetType);
		params.setTaskId(getTaskId());
		
		IIsoProvider provider = ResourceProviders.getDefault(ResourceType.ISO);
		provider.distributeIso(params);
		return null;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return 1200;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return CheckAndDistributeIsoCacheFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return CheckAndDistributeIsoCacheFlowCommandRes.class;
	}

}