package com.gcloud.controller.compute.handler.api.vm.zone;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.model.vm.EnableZoneParams;
import com.gcloud.controller.compute.service.vm.zone.IVmZoneService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.zone.ApiEnableZoneMsg;
import com.gcloud.header.compute.msg.api.vm.zone.ApiEnableZoneReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule = SubModule.VM, action = "EnableZone", name = "启用/禁用可用�?")
public class ApiEnableZoneHandler extends MessageHandler<ApiEnableZoneMsg, ApiEnableZoneReplyMsg>{

	@Autowired
    private IVmZoneService zoneService;
	
	@Override
	public ApiEnableZoneReplyMsg handle(ApiEnableZoneMsg msg) throws GCloudException {
		EnableZoneParams params = BeanUtil.copyProperties(msg, EnableZoneParams.class);
		zoneService.enableZone(params);
		
		ApiEnableZoneReplyMsg reply = new ApiEnableZoneReplyMsg();
		return reply;
	}

}