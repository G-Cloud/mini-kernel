package com.gcloud.controller.compute.handler.api.vm.network;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.workflow.model.network.DetachPortInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.model.network.DetachPortWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.network.DetachPortWorkflow;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.network.ApiDetachNetworkInterfaceMsg;
import com.gcloud.header.log.model.Task;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@LongTask
@GcLog(isMultiLog = true, taskExpect = "卸载网卡")
@ApiHandler(module= Module.ECS, subModule = SubModule.NETWORKINTERFACE, action="DetachNetworkInterface")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.PORT, resourceIdField = "networkInterfaceId")
public class ApiDetachNetworkInterfaceHandler extends BaseWorkFlowHandler<ApiDetachNetworkInterfaceMsg, ApiReplyMessage> {

    @Override
    public Object preProcess(ApiDetachNetworkInterfaceMsg msg) throws GCloudException {

        return null;
    }

    @Override
    public ApiReplyMessage process(ApiDetachNetworkInterfaceMsg msg) throws GCloudException {
        ApiReplyMessage replyMessage = new ApiReplyMessage();
        DetachPortInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), DetachPortInitFlowCommandRes.class);
        replyMessage.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getInstanceId()).objectName(CacheContainer.getInstance().getString(CacheType.INSTANCE_ALIAS, msg.getInstanceId())).expect("卸载网卡成功").build());
        return replyMessage;
    }

    @Override
    public Class getWorkflowClass() {
        return DetachPortWorkflow.class;
    }

    @Override
    public Object initParams(ApiDetachNetworkInterfaceMsg msg) {
        DetachPortWorkflowReq req = new DetachPortWorkflowReq();
        req.setInstanceId(msg.getInstanceId());
        req.setNetworkInterfaceId(msg.getNetworkInterfaceId());
        req.setInTask(false);
        return req;
    }
}