package com.gcloud.controller.compute.workflow.vm.iso;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.compute.ControllerComputeProp;
import com.gcloud.controller.compute.workflow.model.iso.RbdmapAndLnIsoCacheFlowCommandReq;
import com.gcloud.controller.image.dao.IsoMapInfoDao;
import com.gcloud.controller.image.entity.IsoMapInfo;
import com.gcloud.controller.storage.dao.StoragePoolDao;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.enums.IsoMapStatus;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.compute.msg.node.vm.iso.RbdmapAndLnIsoCacheMsg;
import com.gcloud.header.storage.enums.StoragePoolDriver;
import com.gcloud.service.common.compute.uitls.VmUtil;
import com.gcloud.service.common.enums.RbdMapAction;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class RbdmapAndLnIsoCacheFlowCommand extends BaseWorkFlowCommand{
	@Autowired
    private StoragePoolDao storagePoolDao;
	
	@Autowired
    private IsoMapInfoDao isoMapInfoDao;
	
	@Autowired
    private MessageBus bus;
	
	@Autowired
    private ControllerComputeProp prop;
	
	@Override
	public boolean judgeExecute() {
		RbdmapAndLnIsoCacheFlowCommandReq req = (RbdmapAndLnIsoCacheFlowCommandReq)getReqParams();
		StoragePool pool = this.storagePoolDao.getById(req.getStoragePoolId());
		if(!pool.getStorageType().equals(StorageType.DISTRIBUTED.getValue())) {
			return false;
		}
		
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("hostname", req.getHostname());
		props.put("isoId", req.getIsoId());
		IsoMapInfo isoMap = isoMapInfoDao.findUniqueByProperties(props);
		/*if(isoMap != null && !isoMap.getStatus().equals(IsoMapStatus.MAPPED.name())) {
			throw new GCloudException("::映像在mapping中，请等待mapping完成后再尝试操作");
		}*/
		
		return isoMap == null || !isoMap.getStatus().equals(IsoMapStatus.MAPPED.name());
	}
	
	@Override
	protected Object process() throws Exception {
		RbdmapAndLnIsoCacheFlowCommandReq req = (RbdmapAndLnIsoCacheFlowCommandReq)getReqParams();
		String rbdMapAction = RbdMapAction.MAP.value();
		try {
			IsoMapInfo isoMap = new IsoMapInfo();
			isoMap.setId(String.format("%s_%s_%s", prop.getCephIsoPool(), req.getHostname(), req.getIsoId()));
			isoMap.setHostname(req.getHostname());
			isoMap.setIsoId(req.getIsoId());
			isoMap.setPoolName(prop.getCephIsoPool());
			isoMap.setLnPath(VmUtil.getIsoPath(req.getIsoId(), prop.getCephIsoPool(), StoragePoolDriver.RBD.name()));
			isoMap.setStatus(IsoMapStatus.MAPPING.name());
			isoMap.setCreatedAt(new Date());
			
			isoMapInfoDao.save(isoMap);
		}catch(Exception ex) {
			rbdMapAction = RbdMapAction.CHECK.value();
		}
		
		RbdmapAndLnIsoCacheMsg msg = new RbdmapAndLnIsoCacheMsg();
		msg.setIsoId(req.getIsoId());
		msg.setPoolName(prop.getCephIsoPool());
		msg.setHostname(req.getHostname());
		msg.setRbdMapAction(rbdMapAction);
		msg.setTaskId(getTaskId());
		msg.setServiceId(MessageUtil.computeServiceId(req.getHostname()));
		
		bus.send(msg);
		
		return null;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return RbdmapAndLnIsoCacheFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return null;
	}

}