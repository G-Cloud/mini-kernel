package com.gcloud.controller.compute.handler.api.vm.base.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiStartInstanceMsg;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiStartInstanceReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS,subModule=SubModule.VM, action = "StartInstance", versions={ApiVersion.Standard})
@LongTask
@GcLog(taskExpect = "启动云服务器")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
public class StandardApiStartInstanceHandler extends MessageHandler<StandardApiStartInstanceMsg, StandardApiStartInstanceReplyMsg>{

	@Autowired
	private IVmBaseService vmBaseService;
	
	@Override
	public StandardApiStartInstanceReplyMsg handle(StandardApiStartInstanceMsg msg) throws GCloudException {
		vmBaseService.startInstance(msg.getInstanceId(), false, true);
		StandardApiStartInstanceReplyMsg reply = new StandardApiStartInstanceReplyMsg();

        msg.setInstanceId(msg.getInstanceId());
        msg.setObjectName(CacheContainer.getInstance().getString(CacheType.INSTANCE_ALIAS, msg.getInstanceId()));

        return reply;
	}

}