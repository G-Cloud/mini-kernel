package com.gcloud.controller.network.dao;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.entity.Router;
import com.gcloud.controller.network.entity.Subnet;
import com.gcloud.controller.network.model.DescribeVRoutersParams;
import com.gcloud.controller.utils.SqlUtil;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationType;
import com.gcloud.core.currentUser.policy.enums.UserResourceFilterPolicy;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.core.currentUser.policy.service.IUserResourceFilterPolicy;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.api.model.CurrentUser;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Repository
public class RouterDao  extends JdbcBaseDaoImpl<Router, String>{

	public <E> PageResult<E> describeVRouters(DescribeVRoutersParams params, Class<E> clazz, CurrentUser currentUser){
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "r.", ResourceIsolationCheckType.ROUTER);
		List<Object> values = new ArrayList<>();
		
		StringBuffer sql = new StringBuffer();

//        sql.append("select r.id as vRouterId,r.name as vRouterName,r.region_id,r.status as regionId from gc_routers r where 1 = 1 ");
		sql.append("select r.id, r.user_id, r.create_time, max(r.external_gateway_network_id) external_gateway_network_id, max(r.status) status, max(r.region_id) region_id, max(r.name) name, group_concat(distinct i.subnet_id) subnets");
		sql.append(" from gc_routers r left join gc_router_ports rp on r.id = rp.router_id");
		sql.append(" left join gc_ipallocations i on rp.port_id = i.port_id");
		sql.append(" where 1 = 1");   
		
		if(StringUtils.isNotBlank(params.getExternalGatewayNetworkId())) {
			sql.append(" and r.external_gateway_network_id = ?");
			values.add(params.getExternalGatewayNetworkId());
		}
		
		if(StringUtils.isNotBlank(params.getKey())) {
			sql.append(" and r.name like concat('%', ?, '%')");
			values.add(params.getKey());
		}
		
		//可能造成sql过长，导致执行失�?
		if(params.getVpcIds() != null && !params.getVpcIds().isEmpty()) {
			sql.append(" and r.id in (").append(SqlUtil.inPreStr(params.getVpcIds().size())).append(") ");
			values.addAll(params.getVpcIds());
		}
		
        sql.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());
		sql.append(" group by r.id");
        sql.append(" order by r.create_time desc");

        return findBySql(sql.toString(), values, params.getPageNumber(), params.getPageSize(), clazz);
	}
	
	public int routerStatistics(CurrentUser currentUser) {
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "r.", ResourceIsolationCheckType.ROUTER);
		List<Object> values = new ArrayList<>();
		
		StringBuffer sql = new StringBuffer();
		sql.append("select count(r.id) from gc_routers r where 1=1 ");
		sql.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());
		return countBySql(sql.toString(), values);
	}
	
	public List<Subnet> getSubnetsByRouterId(String routerId) {
		StringBuffer sql = new StringBuffer();
		List<Object> values = new ArrayList<>();
		
		sql.append("select * from gc_subnets as gs where network_id in( ");
		sql.append(" select gn.id from gc_routers as gr ");
		sql.append(" left join gc_networks as gn on CONCAT('vpcId:',gr.id,'-network') = gn.name ");
		sql.append(" where gr.id = ?)");
		values.add(routerId);
		
		return findBySql(sql.toString(), values, Subnet.class);
	}
}