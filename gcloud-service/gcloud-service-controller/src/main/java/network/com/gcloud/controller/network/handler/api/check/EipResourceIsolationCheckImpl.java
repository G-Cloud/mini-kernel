package com.gcloud.controller.network.handler.api.check;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.controller.network.dao.FloatingIpDao;
import com.gcloud.controller.network.entity.FloatingIp;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationType;
import com.gcloud.core.currentUser.policy.service.ResourceIsolationCheckImpl;
import com.gcloud.header.common.ResourceOwner;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Service
public class EipResourceIsolationCheckImpl extends ResourceIsolationCheckImpl {
	@Autowired
    FloatingIpDao floatingipDao;; 
	
	@Override
	public ResourceOwner getResourceTenantId(String resourceId) {
		FloatingIp floatingIp = floatingipDao.getById(resourceId);
		ResourceOwner resourceOwner = null;
		if(floatingIp != null) {
			resourceOwner = new ResourceOwner(floatingIp.getUserId(), floatingIp.getTenantId());
		}
		return resourceOwner;
	}
	
	@Override
	public ResourceIsolationType getIsolationType() {
		return ResourceIsolationType.IN_TENANT_SELF;
	}

}