package com.gcloud.controller.network.service;

import com.gcloud.controller.network.model.CreateNetworkParams;
import com.gcloud.controller.network.model.DetailExternalNetworkParams;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.network.model.DetailExternalNetwork;
import com.gcloud.header.network.model.ExternalNetworkSetType;
import com.gcloud.header.network.model.TplExternalNetworkResponse;
import com.gcloud.header.network.model.VpcsItemType;
import com.gcloud.header.network.msg.api.ApiTplExternalNetworksMsg;
import com.gcloud.header.network.msg.api.CreateExternalNetworkMsg;
import com.gcloud.header.network.msg.api.DescribeExternalNetworksMsg;
import com.gcloud.header.network.msg.api.DescribeVpcsMsg;
import com.gcloud.header.network.msg.api.ModifyVpcAttributeMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface INetworkService {
	PageResult<VpcsItemType> describeVpcs(DescribeVpcsMsg msg);
	String createNetwork(CreateNetworkParams params, CurrentUser currentUser);
	String createExternalNetwork(CreateExternalNetworkMsg msg);
	PageResult<ExternalNetworkSetType> describeNetworks(DescribeExternalNetworksMsg msg);
	void removeNetwork(String networkId);
	void updateNetwork(ModifyVpcAttributeMsg msg);
	void getNetworks(String id);
	DetailExternalNetwork detailExternalNetwork(DetailExternalNetworkParams params, CurrentUser currentUser);
	TplExternalNetworkResponse tplExternalNetworks(ApiTplExternalNetworksMsg msg);
}