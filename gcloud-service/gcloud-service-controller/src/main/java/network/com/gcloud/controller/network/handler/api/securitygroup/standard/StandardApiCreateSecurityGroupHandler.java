package com.gcloud.controller.network.handler.api.securitygroup.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.model.CreateSecurityGroupParams;
import com.gcloud.controller.network.service.ISecurityGroupService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.standard.StandardApiCreateSecurityGroupMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiCreateSecurityGroupReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@GcLog(taskExpect="新建安全�?")
@ApiHandler(module=Module.ECS,subModule=SubModule.SECURITYGROUP,action="CreateSecurityGroup", versions = {ApiVersion.Standard})
public class StandardApiCreateSecurityGroupHandler extends MessageHandler<StandardApiCreateSecurityGroupMsg, StandardApiCreateSecurityGroupReplyMsg>{

	@Autowired
	ISecurityGroupService securityGroupService;
	
	@Override
	public StandardApiCreateSecurityGroupReplyMsg handle(StandardApiCreateSecurityGroupMsg msg) throws GCloudException {
		CreateSecurityGroupParams params = BeanUtil.copyProperties(msg, CreateSecurityGroupParams.class);
		StandardApiCreateSecurityGroupReplyMsg reply = new StandardApiCreateSecurityGroupReplyMsg();
		reply.setSecurityGroupId(securityGroupService.createSecurityGroup(params, msg.getCurrentUser()));
		msg.setObjectName(params.getSecurityGroupName());
		return reply;
	}

}