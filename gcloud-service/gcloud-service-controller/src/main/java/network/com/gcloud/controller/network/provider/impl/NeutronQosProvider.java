package com.gcloud.controller.network.provider.impl;

import com.gcloud.controller.network.provider.IQosProvider;
import com.gcloud.controller.network.provider.enums.neutron.NeutronQosDirection;
import com.gcloud.controller.provider.NeutronProviderProxy;
import com.gcloud.header.compute.enums.QosDirection;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import org.openstack4j.api.Builders;
import org.openstack4j.model.network.NetFloatingIPUpdate;
import org.openstack4j.model.network.Port;
import org.openstack4j.model.network.QosBandwidthLimitRule;
import org.openstack4j.model.network.QosPolicy;
import org.openstack4j.model.network.builder.QosPolicyBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
public class NeutronQosProvider implements IQosProvider {

    @Autowired
    private NeutronProviderProxy neutronProviderProxy;

    @Override
    public ResourceType resourceType() {
        return ResourceType.QOS;
    }

    @Override
    public ProviderType providerType() {
        return ProviderType.NEUTRON;
    }

    @Override
    public String createQosPolicy(String name, String description, Boolean isDefault, Boolean shared) {

        QosPolicyBuilder qosPolicyBuilder = Builders.qosPolicy();
        if(name != null){
            qosPolicyBuilder.name(name);
        }
        if(description != null){
            qosPolicyBuilder.description(description);
        }
        if(isDefault != null){
            qosPolicyBuilder.isDefault(isDefault);
        }
        if(shared != null){
            qosPolicyBuilder.shared(shared);
        }
        QosPolicy policy = neutronProviderProxy.createQosPolicy(qosPolicyBuilder.build());
        return policy.getId();
    }

    @Override
    public void deleteQosPolicy(String refId) {
        neutronProviderProxy.deleteQosPolicy(refId);
    }


    @Override
    public String createQosBandwidthLimitRule(String policyRefId, Integer maxKbps, Integer maxBurstKbps, QosDirection qosDirection) {
        QosBandwidthLimitRule rule = neutronProviderProxy.createQosBandwidthLimitRule(policyRefId, maxKbps, maxBurstKbps, NeutronQosDirection.getByGcValue(qosDirection));
        return rule.getId();
    }

    @Override
    public void updateQosBandwidthLimitRule(String policyRefId, String ruleRefId, Integer maxKbps, Integer maxBurstKbps, QosDirection qosDirection) {
        neutronProviderProxy.updateQosBandwidthLimitRule(policyRefId, ruleRefId, maxKbps, maxBurstKbps, NeutronQosDirection.getByGcValue(qosDirection));
    }

    @Override
    public void deleteQosBandwidthLimitRule(String policyRefId, String ruleRefId) {
        neutronProviderProxy.deleteQosBandwidthLimitRule(policyRefId, ruleRefId);
    }

    @Override
    public void bindPort(String portRefId, String policyRefId) {
        Port updatePort = Builders.port().qosPolicyId(policyRefId).build();
        updatePort.setId(portRefId);
        neutronProviderProxy.updatePort(updatePort);
    }

    @Override
    public void bindFip(String fipRefId, String policyRefId) {
        NetFloatingIPUpdate update = Builders.netFloatingIPUpdate().id(fipRefId).qosPolicyId(policyRefId).build();
        neutronProviderProxy.updateFloatingIp(update);
    }
}