package com.gcloud.controller.network.service.impl;

import com.gcloud.common.util.NetworkUtil;
import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.network.dao.IpallocationPoolDao;
import com.gcloud.controller.network.dao.NetworkDao;
import com.gcloud.controller.network.dao.PortDao;
import com.gcloud.controller.network.dao.SubnetDao;
import com.gcloud.controller.network.entity.IpallocationPools;
import com.gcloud.controller.network.entity.Network;
import com.gcloud.controller.network.entity.Port;
import com.gcloud.controller.network.entity.Subnet;
import com.gcloud.controller.network.enums.NetworkType;
import com.gcloud.controller.network.model.CreateSubnetParams;
import com.gcloud.controller.network.model.DescribeSubnetParams;
import com.gcloud.controller.network.provider.ISubnetProvider;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.controller.network.util.NeutronSubnetDhcpUtil;
import com.gcloud.controller.utils.OrderBy;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.DeviceOwner;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.network.model.AllocationPool;
import com.gcloud.header.network.model.DescribeSubnetModel;
import com.gcloud.header.network.model.DetailSubnet;
import com.gcloud.header.network.msg.api.ModifySubnetAttributeMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Service
@Slf4j
@Transactional(propagation = Propagation.REQUIRED)
public class SubnetServiceImpl implements ISubnetService {
    @Autowired
    private SubnetDao subnetDao;
    @Autowired
    private NetworkDao networkDao;
    @Autowired
    private PortDao portDao;

    @Autowired
    private IPortService portService;

    @Autowired
    private IpallocationPoolDao ipallocationPoolDao;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String createSubnet(CreateSubnetParams params, CurrentUser currentUser) {
        Network network = networkDao.getById(params.getNetworkId());
        if (network == null) {
            throw new GCloudException("0030402::找不到网�?");
        }

        if(params.getAllocationPools() != null && params.getAllocationPools().size() > 0){

            List<String> ipRanges = new ArrayList<>();
            for(AllocationPool pool : params.getAllocationPools()){
                if(StringUtils.isBlank(pool.getStart()) || StringUtils.isBlank(pool.getEnd()) || !NetworkUtil.isIpv4(pool.getStart()) || !NetworkUtil.isIpv4(pool.getEnd()) || !NetworkUtil.checkCidrIp(params.getCidrBlock(), pool.getStart()) || !NetworkUtil.checkCidrIp(params.getCidrBlock(), pool.getEnd())){
                    throw new GCloudException("0030205::�?输入的IP不在子网范围�?");
                }

                if(NetworkUtil.ipToDecm(pool.getStart()) > NetworkUtil.ipToDecm(pool.getEnd())){
                    throw new GCloudException("0030207::IP结束地址要不能小于开始地�?");
                }

                ipRanges.add(String.format("%s-%s", pool.getStart(), pool.getEnd()));

                if(NetworkUtil.ipRangeConflict(ipRanges)){
                    throw new GCloudException("0030206::IP范围冲突");
                }
            }

        }

        //校验dns
        if(params.getDnsNameServers() != null && params.getDnsNameServers().size() > 0){
            for(String dns : params.getDnsNameServers()){
                if(!NetworkUtil.isIpv4(dns)){
                    throw new GCloudException("0030208::DNS无效");
                }
            }
        }

        String subnetId = StringUtils.genUuid();
        this.getProviderOrDefault().createSubnet(network, subnetId, params, currentUser);
        return subnetId;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteSubnet(String subnetId) {
        Subnet subnet = subnetDao.getById(subnetId);
        if (null == subnet) {
            throw new GCloudException("0030402::找不到交换机");
        }
        subnetDao.deleteById(subnetId);
        //删除dhcp的poort
        ipallocationPoolDao.deleteBySubnetId(subnetId);

        List<Port> ports = portDao.subnetPorts(subnetId);
        if(ports != null && ports.size() > 0){
            for(Port port : ports){
                if(!DeviceOwner.ROUTER.getValue().equals(port.getDeviceOwner()) && !DeviceOwner.HA_ROUTER.getValue().equals(port.getDeviceOwner()) && !DeviceOwner.DHCP.getValue().equals(port.getDeviceOwner())){
                    throw new GCloudException("0030403::有关联的端口，不能删�?");
                }
            }

            for(Port port : ports){
                portService.cleanPortData(port.getId());
            }
        }

        this.checkAndGetProvider(subnet.getProvider()).deleteSubnet(subnet.getProviderRefId());
        try{
            NeutronSubnetDhcpUtil.deleteHandle(subnetId, subnet.getProviderRefId());
        }catch (Exception ex){
            log.error("subnet dhcp handle delete fail:" + ex, ex);
        }

    }

    @Override
    public void modifyAttribute(String subnetId, String subnetName, CurrentUser currentUser) {
        Subnet subnet = subnetDao.getById(subnetId);
        if (null == subnet) {
            throw new GCloudException("0030203::找不到交换机");
        }
        //如果要修改dhcp，需要清楚数据库dhcp的port
        ModifySubnetAttributeMsg msg = new ModifySubnetAttributeMsg();
        msg.setSubnetId(subnetId);
        msg.setSubnetName(subnetName);
        msg.setCurrentUser(currentUser);
        modifyAttribute(msg);
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void modifyAttribute(ModifySubnetAttributeMsg params) {
        Subnet subnet = subnetDao.getById(params.getSubnetId());
        if (null == subnet) {
            throw new GCloudException("0170203::找不到该子网");
        }

        Subnet updateSubnet = new Subnet();
        updateSubnet.setId(subnet.getId());

        List<String> updateField = new ArrayList<String>();
        updateField.add(updateSubnet.updateName(params.getSubnetName()));
        updateField.add(updateSubnet.updateUpdatedAt(new Date()));
        //dns
        if (params.getDnsNameServers() != null) {
        	String dnsServers = "";
			for (String host : params.getDnsNameServers()) {
				dnsServers += host + ";";
			}
			if(StringUtils.isNotBlank(dnsServers)) {
				updateField.add(updateSubnet.updateDnsServers(dnsServers.substring(0, dnsServers.length() - 1)));
			}
		}
        if(com.gcloud.common.util.StringUtils.isNotBlank(params.getGatewayIp())) {
        	updateField.add(updateSubnet.updateGatewayIp(params.getGatewayIp()));
        }


        if(params.getDhcp() != null){
            updateField.add(updateSubnet.updateEnableDhcp(params.getDhcp()));
        }

        //启用dhcp，在provider里面处理

        subnetDao.update(updateSubnet, updateField);

        CacheContainer.getInstance().put(CacheType.SUBNET_NAME, params.getSubnetId(), params.getSubnetName());

        this.checkAndGetProvider(subnet.getProvider()).modifyAttribute(subnet, params.getSubnetName(),params.getDnsNameServers(), params.getGatewayIp(), params.getDhcp(), params.getCurrentUser());

        //取消dhcp
        if(params.getDhcp() != null && subnet.getEnableDhcp() && !params.getDhcp()){
            try{
                NeutronSubnetDhcpUtil.createOrUpdateHandle(subnet.getId(), subnet.getProviderRefId());
            }catch (Exception ex){
                log.error("subnet dhcp handle update fail:" + ex, ex);
            }

        }

    }


	@Override
	public PageResult<DescribeSubnetModel> describeSubnets(DescribeSubnetParams params, CurrentUser currentUser) {
		//TODO tenantName, userName, vrouterId没有赋�??
		Integer networkType = params.getNetworkType();
		if(null != networkType) {
			if(networkType.intValue() != 0 && networkType.intValue() != 1) {
				log.error("::网络类型错误");
				throw new GCloudException("0170501::网络类型错误");
			}
		}
		
		PageResult<DescribeSubnetModel> response = subnetDao.describeSubnets(params, DescribeSubnetModel.class, currentUser);
		return response;
	}

    @Override
    public DetailSubnet detail(String subnetId) {

        DetailSubnet subnet = subnetDao.getById(subnetId, DetailSubnet.class);
        if(subnet == null){
            throw new GCloudException("0170402::子网不存�?");
        }

        List<AllocationPool> pools = ipallocationPoolDao.findByProperty(IpallocationPools.SUBNET_ID, subnetId, IpallocationPools.FIRST_IP, OrderBy.OrderType.ASC.value(), AllocationPool.class);
        subnet.setAllocationPools(pools);
        return subnet;
    }

    private ISubnetProvider getProviderOrDefault() {
        ISubnetProvider provider = ResourceProviders.getDefault(ResourceType.SUBNET);
        return provider;
    }

    private ISubnetProvider checkAndGetProvider(Integer providerType) {
        ISubnetProvider provider = ResourceProviders.checkAndGet(ResourceType.SUBNET, providerType);
        return provider;
    }

	@Override
	public void subnetIsolationCheck(String subnetId, CurrentUser currentUser) {
		Subnet subnet = subnetDao.getById(subnetId);
		Network network = networkDao.getById(subnet.getNetworkId());
        if (network == null) {
            throw new GCloudException("::找不到网�?");
        }
        if(network.getType().equals(0)) {//内部网络
        	ResourceIsolationCheckType.SUBNET.getCheck().check(subnetId, currentUser);
        }
	}

}