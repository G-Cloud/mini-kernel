package com.gcloud.controller.network.handler.api.subnet.standard;

import com.gcloud.controller.ResourceIsolationCheck;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.model.DescribeVSwitchesParams;
import com.gcloud.controller.network.service.ISwitchService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.ApiUtil;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.VSwitchSetType;
import com.gcloud.header.network.model.standard.StandardVSwitchSetType;
import com.gcloud.header.network.msg.api.standard.StandardApiDescribeVSwitchesMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiDescribeVSwitchesReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.VSWITCH,action="DescribeVSwitches", versions = {ApiVersion.Standard}, name = "交换机列�?")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.ROUTER, resourceIdField = "vpcId")
public class StandardApiDescribeVSwitchesHandler extends MessageHandler<StandardApiDescribeVSwitchesMsg, StandardApiDescribeVSwitchesReplyMsg>{

	@Autowired
	private ISwitchService subnetService;
	
	@Override
	public StandardApiDescribeVSwitchesReplyMsg handle(StandardApiDescribeVSwitchesMsg msg) throws GCloudException {
		DescribeVSwitchesParams params = BeanUtil.copyProperties(msg, DescribeVSwitchesParams.class);
		
		PageResult<VSwitchSetType> response = subnetService.describeVSwitches(params, msg.getCurrentUser());
		PageResult<StandardVSwitchSetType> stdResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));
		
		StandardApiDescribeVSwitchesReplyMsg replyMsg = new StandardApiDescribeVSwitchesReplyMsg();
        replyMsg.init(stdResponse);
        return replyMsg;
	}
	
	private List<StandardVSwitchSetType> toStandardReply(List<VSwitchSetType> list) {
		if(null == list) {
			return null;
		}
		
		List<StandardVSwitchSetType> stdList = new ArrayList<>();
		
		for (VSwitchSetType item : list) {
			StandardVSwitchSetType tmp = BeanUtil.copyProperties(item, StandardVSwitchSetType.class);
			stdList.add(tmp);
		}
		return stdList;
	}

}