package com.gcloud.controller.network.handler.api.securitygroup.standard;

import com.gcloud.controller.network.model.DescribeSecurityGroupsParams;
import com.gcloud.controller.network.service.ISecurityGroupService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.ApiUtil;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.SecurityGroupItemType;
import com.gcloud.header.network.model.standard.StandardSecurityGroupItemType;
import com.gcloud.header.network.msg.api.standard.StandardApiDescribeSecurityGroupsMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiDescribeSecurityGroupsReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.SECURITYGROUP,action="DescribeSecurityGroups", versions = {ApiVersion.Standard}, name = "安全组列�?")
public class StandardApiDescribeSecurityGroupsHandler extends MessageHandler<StandardApiDescribeSecurityGroupsMsg, StandardApiDescribeSecurityGroupsReplyMsg>{

	@Autowired
	ISecurityGroupService securityGroupService;
	
	@Override
	public StandardApiDescribeSecurityGroupsReplyMsg handle(StandardApiDescribeSecurityGroupsMsg msg)
			throws GCloudException {
		DescribeSecurityGroupsParams params = BeanUtil.copyProperties(msg, DescribeSecurityGroupsParams.class);
		//vpcId在查询的时�?�并没有获取
        PageResult<SecurityGroupItemType> response = securityGroupService.describeSecurityGroups(params, msg.getCurrentUser());
        PageResult<StandardSecurityGroupItemType> stdResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));
        
        StandardApiDescribeSecurityGroupsReplyMsg replyMsg = new StandardApiDescribeSecurityGroupsReplyMsg();
        replyMsg.init(stdResponse);
        return replyMsg;
	}
	
	private List<StandardSecurityGroupItemType> toStandardReply(List<SecurityGroupItemType> list) {
		if(null == list) {
			return null;
		}
		
		List<StandardSecurityGroupItemType> stdList = new ArrayList<>();
		
		for (SecurityGroupItemType item : list) {
			StandardSecurityGroupItemType tmp = BeanUtil.copyProperties(item, StandardSecurityGroupItemType.class);
			//无法进行copyPropertites,参数名称不一�?
			tmp.setCreationTime(item.getCreateTime());
			stdList.add(tmp);
		}
		return stdList;
		
	}

}