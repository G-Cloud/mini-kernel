package com.gcloud.controller.network.dao;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.entity.FloatingIp;
import com.gcloud.controller.utils.SqlUtil;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.UserResourceFilterPolicy;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.core.currentUser.policy.service.IUserResourceFilterPolicy;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.DeviceOwner;
import com.gcloud.header.network.model.EipAddressSetType;
import com.gcloud.header.network.msg.api.DescribeEipAddressesMsg;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Repository
public class FloatingIpDao extends JdbcBaseDaoImpl<FloatingIp, String>{
	public PageResult<EipAddressSetType> getByPage(DescribeEipAddressesMsg param) {
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(param.getCurrentUser(), "f.", ResourceIsolationCheckType.EIP);
		
		List<Object> values = new ArrayList<Object>();

		StringBuffer sb = new StringBuffer();
		sb.append("select f.id, f.status, f.floating_ip_address, f.create_time, f.floating_network_id, f.fixed_port_id, bw.bandwidth, f.fixed_port_id as instanceId,gp.name as instanceName, ");
		
		//instanceName和externalNetworkName
		sb.append(" gi.id as vmInstanceId, gi.alias as vmInstanceName, gn.name as externalNetworkName");
		
		sb.append(" from gc_floating_ips f ");
		sb.append(" left join(");
		sb.append(" select b.fip_id, convert(ifnull(min(r.max_kbps), 0) / 1024, signed) bandwidth");
		sb.append(" from gc_qos_fip_policy_bindings b, gc_qos_bandwidth_limit_rules r where b.policy_id = r.qos_policy_id group by b.fip_id");
		sb.append(" ) bw");
		sb.append(" on f.id = bw.fip_id");
		
		//补充instanceName和externalNetworkName
		sb.append(" left join gc_ports as gp on f.fixed_port_id = gp.id");

		sb.append(" left join gc_instances gi on gi.id=gp.device_id");
		sb.append(" left join gc_networks as gn on f.floating_network_id = gn.id");

		sb.append(" where 1 = 1");

		if(StringUtils.isNotBlank(param.getAllocationId())) {
			sb.append(" AND f.id = ?");
			values.add(param.getAllocationId());
		}
		if(StringUtils.isNotBlank(param.getEipAddress())) {
			sb.append(" AND f.floating_ip_address = ?");
			values.add(param.getEipAddress());
		}
		if(StringUtils.isNotBlank(param.getStatus())) {
			sb.append(" AND f.status = ?");
			values.add(param.getStatus());
		}
		
		//service层过滤掉了associateInstanceId为ecsInstance的情�?
		//剩下netcard的情况就如此处理
		if(StringUtils.isNotBlank(param.getAssociateInstanceId())) {
			sb.append(" AND f.fixed_port_id = ?");
			values.add(param.getAssociateInstanceId());
		}
		
		if(param.getAvail() != null) {
			if(param.getAvail()) {
				sb.append(" AND f.fixed_port_id is null");
			}else {
				sb.append(" AND f.fixed_port_id is not null");
			}
		}
		
		sb.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());

		sb.append(" ORDER BY f.create_time DESC");
		return this.findBySql(sb.toString(), values, param.getPageNumber(), param.getPageSize(), EipAddressSetType.class);
	}
	
	public int getAllNum(CurrentUser currentUser) {
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "f.", ResourceIsolationCheckType.EIP);
		
		List<Object> values = new ArrayList<Object>();
		StringBuffer countsql = new StringBuffer();
		countsql.append("select count(f.id) from gc_floating_ips f where 1=1 ");
		countsql.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());
		return this.countBySql(countsql.toString(), values);
	}
	
	public <E> List<E> floatingIpStatistics(Class<E> clazz, CurrentUser currentUser){
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "f.",ResourceIsolationCheckType.EIP);
		List<Object> values = new ArrayList<>();
		StringBuffer sql = new StringBuffer();
		
		sql.append("select COALESCE(sum(CASE when f.fixed_port_id IS NOT NULL THEN 1 ELSE 0 END ),0) as used,"); 
		sql.append("COALESCE(sum(CASE when f.fixed_port_id IS NULL THEN 1 ELSE 0 END ),0) as avail "); 
		sql.append("from gc_floating_ips f ");
		sql.append("where 1=1 ");
		
		sql.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());
		
		return findBySql(sql.toString(), values, clazz);
	}
	
	public <E> List<E> getInstancefloatingIps(List<Object> instanceIds, Class<E> clazz){

		List<Object> values = new ArrayList<>();

		StringBuffer sql = new StringBuffer();
		String inStr = SqlUtil.inPreStr(instanceIds.size());
		sql.append("select p.device_id as instanceId,GROUP_CONCAT(f.floating_ip_address) as floatingIpsStr "); 
		sql.append("from gc_ports p LEFT JOIN gc_floating_ips f on p.id=f.fixed_port_id "); 
		sql.append("where p.device_id in (" + inStr + ") ");
		values.addAll(instanceIds);

		List<String> deviceOwnerValues = DeviceOwner.eniDeviceOwnerValues();
		sql.append(" and p.device_owner in (").append(SqlUtil.inPreStr(deviceOwnerValues.size())).append(")");
		values.addAll(deviceOwnerValues);
		
		sql.append("GROUP BY p.device_id ");
		
		return findBySql(sql.toString(), values, clazz);
	}
	
	public List<String> getInstancefloatingIps(String instanceId){
		List<Object> values = new ArrayList<>();
		StringBuffer sql = new StringBuffer();
		sql.append("select f.floating_ip_address");
		sql.append(" from gc_ports p, gc_floating_ips f where p.id=f.fixed_port_id");
		sql.append(" and p.device_id =? ");
		values.add(instanceId);
		
		List<String> deviceOwnerValues = DeviceOwner.eniDeviceOwnerValues();
		sql.append(" and p.device_owner in (").append(SqlUtil.inPreStr(deviceOwnerValues.size())).append(")");
		values.addAll(deviceOwnerValues);

		return this.jdbcTemplate.queryForList(sql.toString(), values.toArray(), String.class);
	}
}