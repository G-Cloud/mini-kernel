package com.gcloud.controller;


import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.log.model.LogFeedbackParams;
import com.gcloud.controller.log.service.ILogService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.currentUser.policy.service.IResourceIsolationCheck;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.handle.MessageHandlerKeeper;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.ApiMessage;
import com.gcloud.header.GMessage;
import com.gcloud.header.NeedReplyMessage;
import com.gcloud.header.NodeMessage;
import com.gcloud.header.ReplyMessage;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Slf4j
public class MessageReceiver {
    @Autowired
    ILogService logService;

    @Value("${spring.rabbitmq.listener.simple.acknowledge-mode:}")
    private String ackMode;

    @RabbitListener(queues = "${gcloud.service.controller}")
    @RabbitHandler
    public ReplyMessage process(Message msg, Channel channel) {

        GMessage message = MessageUtil.ackAndGetMessage(ackMode, msg, channel);
        if(message == null){
            log.error("ack失败或�?�获取队列消息内容失�?");
            ReplyMessage replyMessage = new ReplyMessage(){};
            replyMessage.setSuccess(false);
            replyMessage.setErrorMsg("::消息处理失败");
            return replyMessage;
        }

    	Date startTime = new Date();
        MessageHandler handler = null;
        try {
            handler = MessageHandlerKeeper.get(message.getClass().getName());
            if(handler == null){
                throw new GCloudException("handler not found");
            }

            //资源隔离处理
            if(message instanceof ApiMessage) {
	            ResourceIsolationCheck[] checks = handler.getClass().getAnnotationsByType(ResourceIsolationCheck.class);
	            for(ResourceIsolationCheck check:checks) {
	            	Method method = message.getClass().getMethod(StringUtils.makeGetMethod(check.resourceIdField())); // 父类对象调用子类方法(反射原理)
	            	Object obj = method.invoke(message);
	            	if(obj != null) {
	            		//IResourceIsolationCheck checkService = (IResourceIsolationCheck)SpringUtil.getBean(check.resourceIsolationCheckType().getCheckClazz());
	            		IResourceIsolationCheck checkService = (IResourceIsolationCheck)check.resourceIsolationCheckType().getCheck();
	            		//只支持List<String> �? String
		            	if(obj instanceof List) {
		            		List<String> ids = (List<String>)obj;
		            		for(String id:ids) {
		            			checkService.check(id, ((ApiMessage) message).getCurrentUser());
		            		}
		            	} else if(StringUtils.isNotBlank((String)obj)){
		            		checkService.check((String)obj, ((ApiMessage) message).getCurrentUser());
		            	}
	            	}
	            }

	            //创建资源类接口注解处理，defaultTenant不能为空  ApiTypeAnno
	            ApiTypeAnno apiTypeAnno = (ApiTypeAnno)handler.getClass().getAnnotation(ApiTypeAnno.class);
	            if(apiTypeAnno!=null && apiTypeAnno.apiType().equals(ApiTypeConst.CREATE) && StringUtils.isBlank(((ApiMessage) message).getCurrentUser().getDefaultTenant())) {
	            	throw new GCloudException("::租户不能为空");
	            }
            }

            ReplyMessage reply = handler.handle(message);
            if(reply == null){
                throw new GCloudException("::操作异常");
            }
            log.debug("taskId:" + message.getTaskId() + ",thread id:" + Thread.currentThread().getId());
            GcLog gcLog = (GcLog)handler.getClass().getAnnotation(GcLog.class);
            if (null != gcLog) {
            	if (gcLog.isMultiLog()) {
                    //线程池异步执�?
            		logService.recordMultiLog(message, handler, null, startTime, reply);
                } else {
                    logService.recordLog(message, handler, null, startTime);
                }
            }

            return reply;
        } catch (Throwable e) {
            log.error(String.format("message handle error, ex=%s, message=%s, msgId=%s, serviceId=%s, messageClass=%s", e.toString(), e.getMessage(), message.getMsgId(), message.getServiceId(), message.getClass()), e);

            if(message instanceof NeedReplyMessage){

                Class replyClass = ((NeedReplyMessage)message).replyClazz();
                if(replyClass != null && ReplyMessage.class.isAssignableFrom(replyClass)){

                    try{
                        String errorMsg = "api_controller_error::system error";
                        if(e instanceof GCloudException){
                            errorMsg = e.getMessage();
                        }

                        ReplyMessage replyMessage = (ReplyMessage)replyClass.newInstance();
                        replyMessage.setSuccess(false);
                        replyMessage.setErrorMsg(errorMsg);
                        if(e instanceof GCloudException && ((GCloudException) e).getParams() != null){
                            replyMessage.setErrorParam(((GCloudException) e).getParams());
                        }
                        return replyMessage;

                    }catch (Exception nex){
                        log.error("new api reply instance error", nex);
                    }
                }
            }
        }
        return null;
    }

    @RabbitListener(queues = "${gcloud.service.controller}" + "_async")
    @RabbitHandler
    public void proccessAsync(Message msg, Channel channel) {

        GMessage message = MessageUtil.ackAndGetMessage(ackMode, msg, channel);
        if(message == null){
            log.error("ack失败或�?�获取队列消息内容失�?");
            return;
        }

        try{
            AsyncMessageHandler handler = MessageHandlerKeeper.getAsyncHandler(message.getClass().getName());
            if(handler != null){
                handler.handle(message);
            }else{
                log.error(String.format("handler not found msgId=%s, serviceId=%s, messageClass=%s", message.getMsgId(), message.getServiceId(), message.getClass()));
            }
        }catch (Throwable ex){
            log.error(String.format("message handle error, ex=%s, message=%s, msgId=%s, serviceId=%s, messageClass=%s", ex.toString(), ex.getMessage(), message.getMsgId(), message.getServiceId(), message.getClass()), ex);
        }

        try{
            if(message instanceof NodeMessage && StringUtils.isNotBlank(message.getTaskId())) {
                NodeMessage nodemsg = (NodeMessage)message;
                LogFeedbackParams params = new LogFeedbackParams();
                params.setTaskId(nodemsg.getTaskId());
                params.setStatus(nodemsg.getSuccess()?"COMPLETE":"FAILED");
                params.setCode(nodemsg.getErrorCode());
                logService.feedback(params);
            }
        }catch (Exception ex){
            log.error("::feedback error", ex);
        }

    }


}