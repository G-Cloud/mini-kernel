package com.gcloud.controller.utils;

import com.gcloud.common.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class UserInfo {

    private Map<String, String> userNameMap = new HashMap<>();
    public String userName(String userId){
        if(StringUtils.isBlank(userId)){
            return null;
        }
        String userName = null;
        if(!userNameMap.containsKey(userId)){
            userName = UserUtil.userName(userId);
            userNameMap.put(userId, userName);
        }else{
            userName = userNameMap.get(userId);
        }
        return userName;
    }

}