package com.gcloud.controller.storage.handler.node.pool;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.storage.msg.node.pool.LocalPoolReportMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Handler
@Slf4j
public class LocalPoolReportHandler extends AsyncMessageHandler<LocalPoolReportMsg>{
	@Autowired
	IStoragePoolService storagePoolService;

	@Override
	public void handle(LocalPoolReportMsg msg) {
		log.debug("LocalPoolReportHandler start");
		storagePoolService.reportLocalPool(msg);
	}

}