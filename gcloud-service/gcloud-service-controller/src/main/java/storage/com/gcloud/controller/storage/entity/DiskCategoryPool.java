package com.gcloud.controller.storage.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Table(name = "gc_disk_category_pools", jdbc = "controllerJdbcTemplate")
public class DiskCategoryPool {

    @ID
    private Integer id;
    private String diskCategoryId;
    private String zoneId;
    private String storagePoolId;
    private String hostname;
    private String storageType;

    public static final String ID = "id";
    public static final String DISK_CATEGORY_ID = "diskCategoryId";
    public static final String ZONE_ID = "zoneId";
    public static final String STORAGE_POOL_ID = "storagePoolId";
    public static final String HOSTNAME = "hostname";
    public static final String STORAGE_TYPE = "storageType";
    public String updateId (Integer id){
        this.setId(id);
        return ID;
    }
    public String updateDiskCategoryId (String diskCategoryId){
        this.setDiskCategoryId(diskCategoryId);
        return DISK_CATEGORY_ID;
    }
    public String updateZoneId (String zoneId){
        this.setZoneId(zoneId);
        return ZONE_ID;
    }
    public String updateStoragePoolId (String storagePoolId){
        this.setStoragePoolId(storagePoolId);
        return STORAGE_POOL_ID;
    }
    public String updateHostname (String hostname){
        this.setHostname(hostname);
        return HOSTNAME;
    }
    public String updateStorageType (String storageType){
        this.setStorageType(storageType);
        return STORAGE_TYPE;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDiskCategoryId() {
        return diskCategoryId;
    }

    public void setDiskCategoryId(String diskCategoryId) {
        this.diskCategoryId = diskCategoryId;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getStoragePoolId() {
        return storagePoolId;
    }

    public void setStoragePoolId(String storagePoolId) {
        this.storagePoolId = storagePoolId;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }
}