package com.gcloud.controller.storage.service.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.dao.ZoneDao;
import com.gcloud.controller.compute.entity.AvailableZoneEntity;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.utils.VmControllerUtil;
import com.gcloud.controller.image.dao.ImageDao;
import com.gcloud.controller.image.entity.Image;
import com.gcloud.controller.log.util.LongTaskUtil;
import com.gcloud.controller.storage.dao.DiskCategoryDao;
import com.gcloud.controller.storage.dao.SnapshotDao;
import com.gcloud.controller.storage.dao.StoragePoolDao;
import com.gcloud.controller.storage.dao.VolumeAttachmentDao;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.entity.DiskCategory;
import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.entity.VolumeAttachment;
import com.gcloud.controller.storage.entity.VolumeAttachmentInfo;
import com.gcloud.controller.storage.entity.VolumeExInfo;
import com.gcloud.controller.storage.model.CreateDiskParams;
import com.gcloud.controller.storage.model.CreateDiskResponse;
import com.gcloud.controller.storage.model.CreateVolumeParams;
import com.gcloud.controller.storage.model.DescribeDisksParams;
import com.gcloud.controller.storage.model.DetailDiskParams;
import com.gcloud.controller.storage.provider.IVolumeProvider;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.controller.utils.UserUtil;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.util.ApiUtil;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.Device;
import com.gcloud.header.compute.enums.DiskProtocol;
import com.gcloud.header.compute.enums.StorageDiskProtocol;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.compute.msg.api.model.DiskInfo;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.log.enums.LogType;
import com.gcloud.header.storage.StorageErrorCodes;
import com.gcloud.header.storage.enums.DiskType;
import com.gcloud.header.storage.enums.DiskTypeParam;
import com.gcloud.header.storage.enums.VolumeStatus;
import com.gcloud.header.storage.model.DetailDisk;
import com.gcloud.header.storage.model.DiskCategoryStatisticsItem;
import com.gcloud.header.storage.model.DiskCategoryStatisticsResponse;
import com.gcloud.header.storage.model.DiskItemType;
import com.gcloud.header.storage.model.DiskStatusStatisticsItem;
import com.gcloud.header.storage.model.DiskStatusStatisticsResponse;
import com.gcloud.header.storage.model.VmVolumeDetail;
import com.gcloud.header.storage.msg.api.volume.ApiDisksStatisticsReplyMsg;
import com.gcloud.service.common.Consts;
import com.gcloud.service.common.compute.uitls.DiskUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Transactional(propagation = Propagation.REQUIRED)
@Slf4j
public class VolumeServiceImpl implements IVolumeService {

    @Autowired
    private VolumeDao volumeDao;

    @Autowired
    private StoragePoolDao poolDao;

    @Autowired
    private VolumeAttachmentDao volumeAttachmentDao;
    
    @Autowired
    private InstanceDao instanceDao;

    @Autowired
    private SnapshotDao snapshotDao;

    @Autowired
    private StoragePoolDao storagePoolDao;

    @Autowired
    private DiskCategoryDao diskCategoryDao;

    @Autowired
    private ImageDao imageDao;
    
    @Autowired
    private ZoneDao zoneDao;

    @Autowired
    private IStoragePoolService storagePoolService;

    @Override
    public String create(CreateDiskParams cdp, CurrentUser currentUser) {

        CreateVolumeParams params = BeanUtil.copyProperties(cdp, CreateVolumeParams.class);
        params.setDiskType(DiskType.DATA);

        if (StringUtils.isNotBlank(params.getSnapshotId())) {
            Snapshot snapshot = snapshotDao.getById(params.getSnapshotId());
            if (snapshot == null) {
                throw new GCloudException(StorageErrorCodes.SNAPSHOT_NOT_FOUND);
            }
            Volume vol = volumeDao.getById(snapshot.getVolumeId());
            params.setDiskCategory(vol.getCategory());
        }

        return createVolume(params, currentUser);
    }

    @Override
    public String createVolume(CreateVolumeParams params, CurrentUser currentUser) {
    	if(StringUtils.isBlank(params.getDiskCategory())) {
    		throw new GCloudException(StorageErrorCodes.INPUT_DISK_CATEGORY_ERROR);
    	}
    	//TODO 对磁盘类型进行是否可用校�?
    	DiskCategory dc = diskCategoryDao.getById(params.getDiskCategory());
    	if(null == dc) {
    		log.error("::不存在该磁盘类型");
    		throw new GCloudException("::不存在该磁盘类型");
    	}
    	if(!dc.isEnabled()) {
    		log.error("::该磁盘类型不可用");
    		throw new GCloudException("::该磁盘类型不可用");
    	}
    	
    	//TODO 可用�?
    	AvailableZoneEntity zone = zoneDao.getById(params.getZoneId());
    	if(null == zone) {
    		log.error("::该可用区不存�?");
    		throw new GCloudException("::该可用区不存�?");
    	}
    	if(!zone.isEnabled()) {
    		log.error("::该可用区不可�?");
    		throw new GCloudException("::该可用区不可�?");
    	}

        StoragePool pool = null;
    	if(StringUtils.isNotBlank(params.getPoolId())){
            pool = storagePoolDao.getById(params.getPoolId());
        }else{
            pool = storagePoolService.assignStoragePool(params.getDiskCategory(), params.getZoneId(), params.getCreateHost());
        }

    	if(pool == null){
    	    throw new GCloudException("::获取存储池失�?");
        }

        params.setPool(pool);
        if (StringUtils.isNotBlank(params.getImageRef())) {
            Image image = imageDao.getById(params.getImageRef());
            if (image == null) {
                throw new GCloudException(StorageErrorCodes.REF_IMAGE_IS_NOT_FOUND);
            }

            params.setImageProvider(image.getProvider());
            params.setImageProviderRefId(image.getProviderRefId());
        }
        else if (StringUtils.isNotBlank(params.getSnapshotId())) {
            Snapshot snapshot = snapshotDao.checkAndGet(params.getSnapshotId());
            params.setSize(snapshot.getVolumeSize());
        }

        String volumeId = StringUtils.genUuid();
        try {
        	log.debug(String.format("create volume %s request start %s,线程ID�?%s", volumeId, new Date().getTime(),Thread.currentThread().getId()));
	        CreateDiskResponse response = this.getProvider(params.getPool().getProvider()).createVolume(volumeId, params, currentUser);
	        log.debug(String.format("create volume %s request end %s", volumeId, new Date().getTime()));
	        LongTaskUtil.syncSucc(response.getLogType(), params.getTaskId(), volumeId);
        } catch (Exception e) {
            LongTaskUtil.syncFail(LogType.SYNC, params.getTaskId(), volumeId);
        }

        return volumeId;
    }

    @Override
    public void deleteVolume(String volumeId, String taskId) {

        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = this.volumeDao.getById(volumeId);
            if (volume == null) {
                return;
            }

            List<Snapshot> snapshots = snapshotDao.findByProperty(Snapshot.VOLUME_ID, volumeId);
            if (snapshots != null && snapshots.size() > 0) {
                throw new GCloudException("0060315::请清除快照再删除�?");
            }

            this.checkAvailable(volume);
            this.volumeDao.updateVolumeStatus(volumeId, VolumeStatus.DELETING);
        }
        try {
            StoragePool pool = this.poolDao.getById(volume.getPoolId());
            this.getProvider(volume.getProvider()).deleteVolume(pool, volume, taskId);
        } catch (Exception e) {
            this.handleDeleteVolumeFailed(e.getMessage(), volumeId);
            LongTaskUtil.syncFail(LogType.SYNC, taskId, volumeId);
        }
    }

    @Override
    public PageResult<DiskItemType> describeDisks(DescribeDisksParams params, CurrentUser currentUser) {


        if (params == null) {
            params = new DescribeDisksParams();
        }

        if(params.isEmptyList()){
            return ApiUtil.emptyPage(params);
        }

        if (StringUtils.isNotBlank(params.getDiskType())) {
            DiskTypeParam diskType = DiskTypeParam.getByValue(params.getDiskType());
            if (diskType == null) {
                throw new GCloudException(StorageErrorCodes.NO_SUCH_DISK_TYPE);
            }
        }

        PageResult<DiskItemType> volumeList = volumeDao.describeDisks(params, DiskItemType.class, currentUser);
        for (DiskItemType item : volumeList.getList()) {
        	item.setCnStatus(VolumeStatus.getCnName(item.getStatus()));
        	item.setCnType(DiskType.getCnName(item.getType()));
		}
        
        return volumeList;
    }

    @Override
    public void updateVolume(String volumeId, String name) {
        if (StringUtils.isBlank(name)) {
            return;
        }
        Volume volume = volumeDao.checkAndGet(volumeId);

        Volume updateVol = new Volume();
        List<String> updateField = new ArrayList<>();
        updateVol.setId(volumeId);
        updateField.add(updateVol.updateDisplayName(name));
        volumeDao.update(updateVol, updateField);
        CacheContainer.getInstance().put(CacheType.VOLUME_NAME, volumeId, name);

        this.getProvider(volume.getProvider()).updateVolume(volume.getProviderRefId(), name, null);
    }
    
    @Override
    public void updateVolume(String volumeId, String name, String description) {
        if (StringUtils.isBlank(name)) {
            return;
        }
        Volume volume = volumeDao.checkAndGet(volumeId);

        Volume updateVol = new Volume();
        List<String> updateField = new ArrayList<>();
        updateVol.setId(volumeId);
        updateField.add(updateVol.updateDisplayName(name));
        updateField.add(updateVol.updateDescription(description));
        volumeDao.update(updateVol, updateField);
        CacheContainer.getInstance().put(CacheType.VOLUME_NAME, volumeId, name);

        this.getProvider(volume.getProvider()).updateVolume(volume.getProviderRefId(), name, null);
    }

    @Override
    public void reserveVolume(String volumeId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = volumeDao.checkAndGet(volumeId);
            if (volume.getStatus().equals(VolumeStatus.ATTACHING.value())) {
                return;
            }
            this.checkAvailableOrInUse(volume);
            this.volumeDao.updateVolumeStatus(volumeId, VolumeStatus.ATTACHING.value());
        }
        this.getProvider(volume.getProvider()).reserveVolume(volume.getProviderRefId());
    }

    @Override
    public void unreserveVolume(String volumeId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = volumeDao.checkAndGet(volumeId);
            if (!volume.getStatus().equals(VolumeStatus.ATTACHING.value())) {
                return;
            }
            volumeDao.updateVolumeStatus(volumeId, this.volumeAttachmentDao.isAttach(volumeId) ? VolumeStatus.IN_USE.value() : VolumeStatus.AVAILABLE.value());
        }
        this.getProvider(volume.getProvider()).unreserveVolume(volume.getProviderRefId());
    }

    @Override
    public String attachVolume(String volumeId, String instanceId, String mountPoint, String hostname, String taskId) {
        Volume volume;
        String attachmentId = UUID.randomUUID().toString();
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = volumeDao.checkAndGet(volumeId);
//            if (!volume.getStatus().equals(VolumeStatus.ATTACHING.getValue())) {
//                throw new GCloudException(StorageErrorCodes.VOLUME_IS_NOT_ATTACHING);
//            }
            String attachmentRefId = this.getProvider(volume.getProvider()).attachVolume(volume, attachmentId, instanceId, mountPoint, hostname, taskId);
            VolumeAttachment volumeAttachment = new VolumeAttachment();
            volumeAttachment.setId(attachmentId);
            volumeAttachment.setProvider(volume.getProvider());
            volumeAttachment.setProviderRefId(attachmentRefId);
            volumeAttachment.setInstanceUuid(instanceId);
            volumeAttachment.setMountpoint(mountPoint);
            volumeAttachment.setVolumeId(volumeId);
            volumeAttachment.setAttachedHost(hostname);
            volumeAttachmentDao.save(volumeAttachment);

            this.volumeDao.updateVolumeStatus(volumeId, VolumeStatus.IN_USE.value());
        }

        return attachmentId;
    }

    @Override
    public void beginDetachingVolume(String volumeId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = volumeDao.checkAndGet(volumeId);
            this.checkInUse(volume);
            volumeDao.updateVolumeStatus(volumeId, VolumeStatus.DETACHING.value());
        }
        this.getProvider(volume.getProvider()).beginDetachingVolume(volume);
    }

    @Override
    public void rollDetachingVolume(String volumeId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = volumeDao.checkAndGet(volumeId);
            if (!volume.getStatus().equals(VolumeStatus.DETACHING.value())) {
                return;
            }
            this.volumeDao.updateVolumeStatus(volumeId, this.volumeAttachmentDao.isAttach(volumeId) ? VolumeStatus.IN_USE.value() : VolumeStatus.AVAILABLE.value());
        }
        this.getProvider(volume.getProvider()).rollDetachingVolume(volume);
    }

    @Override
    public void detachVolume(String volumeId, String attachmentId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = volumeDao.checkAndGet(volumeId);
            if (!volume.getStatus().equals(VolumeStatus.DETACHING.value())) {
                throw new GCloudException(StorageErrorCodes.VOLUME_IS_NOT_DETACHING);
            }
            VolumeAttachment attachment = this.volumeAttachmentDao.getById(attachmentId);
            if (attachment != null) {
                this.getProvider(volume.getProvider()).detachVolume(volume, attachment);
                this.volumeAttachmentDao.deleteById(attachmentId);
            }
            this.volumeDao.updateVolumeStatus(volumeId, this.volumeAttachmentDao.isAttach(volumeId) ? VolumeStatus.IN_USE.value() : VolumeStatus.AVAILABLE.value());
        }
    }

    @Override
    public void resizeVolume(String volumeId, Integer newSize, String taskId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = this.volumeDao.getById(volumeId);
            if (volume == null) {
                throw new GCloudException(StorageErrorCodes.FAILED_TO_FIND_VOLUME);
            }
            if (volume.getSize() >= newSize) {
                throw new GCloudException(StorageErrorCodes.NEW_SIZE_CANNOT_BE_SMALLER);
            }
            this.checkAvailable(volume);
            if (this.volumeAttachmentDao.isAttach(volumeId)) {
                throw new GCloudException(StorageErrorCodes.VOLUME_IS_ATTACHED);
            }
            this.volumeDao.updateVolumeStatus(volumeId, VolumeStatus.RESIZING);
        }
        try {
            StoragePool pool = this.poolDao.getById(volume.getPoolId());
            this.getProvider(volume.getProvider()).resizeVolume(pool, volume, newSize, taskId);
        }
        catch (Exception e) {
            this.handleResizeVolumeFailed(e.getMessage(), volumeId);
            LongTaskUtil.syncFail(LogType.SYNC, taskId, volumeId);
        }
    }

    @Override
    public Volume getVolume(String volumeId) {
        return volumeDao.findUniqueByProperty("id", volumeId);
    }

    @Override
    public List<VmVolumeDetail> getVolumeInfo(String instanceId) {

        return null;
    }

    @Override
    public VmVolumeDetail volumeDetail(String volumeId, String instanceId) {

        Volume volume = volumeDao.getById(volumeId);
        if (volume == null) {
            return null;
        }

        return volumeDetail(volume, null, instanceId);

    }

    @Override
    public VmVolumeDetail volumeDetail(String instanceId, Device device) {

        Map<String, Object> values = new HashMap<>();
        values.put(VolumeAttachment.INSTANCE_UUID, instanceId);
        values.put(VolumeAttachment.MOUNTPOINT, device.getMountPath());
        VolumeAttachment va = volumeAttachmentDao.findUniqueByProperties(values);
        if (va == null) {
            return null;
        }

        Volume volume = volumeDao.getById(va.getVolumeId());

        return volumeDetail(volume, va, null);
    }

    private VmVolumeDetail volumeDetail(Volume volume, VolumeAttachment volumeAttachment, String instanceId) {

        VmVolumeDetail detail = new VmVolumeDetail();

        if (volumeAttachment == null && StringUtils.isNotBlank(instanceId)) {
            List<VolumeAttachment> volumeAttachments = volumeAttachmentDao.findByVolumeIdAndInstanceId(volume.getId(), instanceId);
            volumeAttachment = volumeAttachments != null && volumeAttachments.size() > 0 ? volumeAttachments.get(0) : null;
        }

        if (volumeAttachment != null) {
            String mountPoint = volumeAttachment.getMountpoint();
            String targetDev = "";
            if (StringUtils.isNotBlank(mountPoint) && mountPoint.lastIndexOf("/") != -1 && !mountPoint.endsWith("/")) {
                targetDev = mountPoint.substring(mountPoint.lastIndexOf("/") + 1);
            }
            detail.setTargetDev(targetDev);

        }
        else if (StringUtils.isNotBlank(instanceId)) {
            String targetDev = VmControllerUtil.getVolumeMountPoint(instanceId);
            detail.setTargetDev(targetDev);
        }

        StoragePool pool = this.storagePoolDao.getById(volume.getPoolId());

        StorageType st = StorageType.value(volume.getStorageType());

        DiskProtocol diskProtocol = DiskProtocol.value(pool.getConnectProtocol());
        if (diskProtocol == null) {
            StorageDiskProtocol storageDiskProtocol = StorageDiskProtocol.getByProviderAndStorageType(volume.getProvider(), pool.getDriver());
            if (storageDiskProtocol == null) {
                throw new GCloudException("0060314::找不到磁盘协�?");
            }
            diskProtocol = storageDiskProtocol.getDiskProtocol();
        }

        detail.setBusType(Consts.DEFAULT_DISK_DRIVER);
        detail.setDiskType(volume.getDiskType());
        detail.setVolumeSize(volume.getSize());
        detail.setVolumeId(volume.getId());
        detail.setVolumeRefId(volume.getProviderRefId());
        detail.setProvider(volume.getProvider());
        detail.setCategory(volume.getCategory());
        detail.setProtocol(diskProtocol.value());

        detail.setStorageType(st.getValue());
        detail.setSourcePath(DiskUtil.volumeSourcePath(volume.getProviderRefId(), pool.getPoolName(), diskProtocol));

        return detail;

    }

    private void checkAvailable(Volume volume) throws GCloudException {
        if (!StringUtils.equals(volume.getStatus(), VolumeStatus.AVAILABLE.value())) {
            throw new GCloudException(StorageErrorCodes.VOLUME_IS_NOT_AVAILABLE);
        }
    }

    private void checkAvailableOrInUse(Volume volume) throws GCloudException {
        if (!StringUtils.equals(volume.getStatus(), VolumeStatus.AVAILABLE.value()) && !StringUtils.equals(volume.getStatus(), VolumeStatus.IN_USE.value())) {
            throw new GCloudException(StorageErrorCodes.VOLUME_IS_NOT_AVAILABLE);
        }
    }

    private void checkInUse(Volume volume) throws GCloudException {
        if (!StringUtils.equals(volume.getStatus(), VolumeStatus.IN_USE.value())) {
            log.debug(String.format("==volume status==volume check in user, volumeId=%s, status=%s", volume.getId(), volume.getStatus()));
            throw new GCloudException(StorageErrorCodes.VOLUME_IS_NOT_IN_USE);
        }
    }

    @Override
    public void handleDeleteVolumeSuccess(String volumeId) {
        this.volumeDao.deleteById(volumeId);
        for (Snapshot snapshot : this.snapshotDao.findByVolume(volumeId)) {
            this.snapshotDao.deleteById(snapshot.getId());
        }
        log.info("删除存储卷成功：{}", volumeId);
    }

    @Override
    public void handleDeleteVolumeFailed(String errorCode, String volumeId) {
        this.volumeDao.updateVolumeStatus(volumeId, VolumeStatus.AVAILABLE);
        log.info("删除存储卷失败：{} -> {}", volumeId, errorCode);
    }

    @Override
    public void handleAttachVolumeSuccess(String volumeId, String attachmentId, String instanceId, String mountPoint, String hostname) {
        Volume volume = volumeDao.getById(volumeId);
        volumeDao.updateVolumeStatus(volumeId, VolumeStatus.IN_USE.value());
        VolumeAttachment volumeAttachment = new VolumeAttachment();
        volumeAttachment.setId(UUID.randomUUID().toString());
        volumeAttachment.setProvider(volume.getProvider());
        volumeAttachment.setProviderRefId(attachmentId);
        volumeAttachment.setInstanceUuid(instanceId);
        volumeAttachment.setMountpoint(mountPoint);
        volumeAttachment.setVolumeId(volumeId);
        volumeAttachment.setAttachedHost(hostname);
        volumeAttachmentDao.save(volumeAttachment);
    }

    @Override
    public void handleResizeVolumeSuccess(String volumeId, int newSize) {
        Volume updateVol = new Volume();
        List<String> updateField = new ArrayList<>();
        updateVol.setId(volumeId);
        updateField.add(updateVol.updateStatus(VolumeStatus.AVAILABLE.value()));
        updateField.add(updateVol.updateSize(newSize));
        this.volumeDao.update(updateVol, updateField);
        log.info("resize存储卷成功：{}", volumeId);
    }

    @Override
    public void handleResizeVolumeFailed(String errorCode, String volumeId) {
        this.volumeDao.updateVolumeStatus(volumeId, VolumeStatus.AVAILABLE);
        log.info("resize存储卷失败：{} -> {}", volumeId, errorCode);
    }

    private IVolumeProvider getProvider(int providerType) {
        IVolumeProvider provider = ResourceProviders.checkAndGet(ResourceType.VOLUME, providerType);
        return provider;
    }

    public void checkCategory(List<DiskInfo> diskInfos) {
        if (diskInfos != null && diskInfos.size() > 0) {
            Map<String, DiskCategory> categoryMap = new HashMap<>();
            for (DiskInfo diskInfo : diskInfos) {
                checkCategory(diskInfo.getCategory(), diskInfo.getSize(), categoryMap);
            }
        }

    }

    public void checkCategory(String category, Integer size) {
        checkCategory(category, size, null);
    }

    private void checkCategory(String category, Integer size, Map<String, DiskCategory> categoryMap) {

        DiskCategory diskCategory = categoryMap == null ? null : categoryMap.get(category);
        if (diskCategory == null) {
            diskCategory = diskCategoryDao.getById(category);
            if (diskCategory == null) {
                throw new GCloudException("0060107::找不到磁盘类�?");
            }
            categoryMap.put(category, diskCategory);
        }

        if ((diskCategory.getMaxSize() != null && size > diskCategory.getMaxSize()) || (diskCategory.getMinSize() != null && size < diskCategory.getMinSize())) {
            throw new GCloudException("0060108::磁盘大小不在磁盘类型允许范围�?");
        }

    }

    @Override
    public ApiDisksStatisticsReplyMsg diskStatistics(CurrentUser currentUser) {
        //为保持磁盘�?�数�?致，统计当前时间之前的disk
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String beforeDate = sdf.format(new Date());
        ApiDisksStatisticsReplyMsg reply = new ApiDisksStatisticsReplyMsg();

        List<DiskCategoryStatisticsItem> categoryStatistics = volumeDao.categoryStatistics(beforeDate, DiskCategoryStatisticsItem.class, currentUser);
        List<DiskStatusStatisticsItem> statusStatistics = volumeDao.statusStatistics(beforeDate, DiskStatusStatisticsItem.class, currentUser);
        reply.setAllNum(statusStatistics.stream().map(DiskStatusStatisticsItem::getCountNum).reduce(0, Integer::sum));

        DiskCategoryStatisticsResponse categoryResponse = new DiskCategoryStatisticsResponse();
        categoryResponse.setItem(categoryStatistics);
        DiskStatusStatisticsResponse statusResponse = new DiskStatusStatisticsResponse();
        statusResponse.setItem(statusStatistics);

        reply.setCategoryStatisticsItems(categoryResponse);
        reply.setStatusStatisticsItems(statusResponse);
        return reply;
    }

	@Override
	public DetailDisk detailDisk(DetailDiskParams params, CurrentUser currentUser) {
		DetailDisk response = new DetailDisk();
		Volume disk = volumeDao.getById(params.getDiskId());
		if(null == disk) {
			return response;
		}
		
		//磁盘基础信息
		response.setId(disk.getId());
		response.setName(disk.getDisplayName());
		response.setSize(disk.getSize());
		response.setCategory(disk.getCategory());
		response.setStatus(disk.getStatus());
		response.setType(disk.getDiskType());
		response.setStorageType(disk.getStorageType());
		response.setCnStatus(VolumeStatus.getCnName(disk.getStatus()));
		response.setCnType(DiskType.getCnName(disk.getDiskType()));
		response.setCreateTime(disk.getCreatedAt());
		response.setDescription(disk.getDescription());

		//存储池相关信�?
		response.setPoolName(disk.getPoolName());
		String poolId = disk.getPoolId();
		if(StringUtils.isNotBlank(poolId)) {
			StoragePool pool = poolDao.getById(poolId);
			response.setPoolDisplayName(pool.getDisplayName());
			response.setPoolHostName(pool.getHostname());
		}
		
		//获取磁盘类型信息
		if(null != disk.getCategory()) {
			DiskCategory category = diskCategoryDao.getById(disk.getCategory());
			if(null != category) {
				response.setCnCategory(category.getName());
			}
		}
		
		//TODO 数据来源?
		response.setDevice(null);
		
		//获取挂载信息
		Map<String, Object> attachParams = new HashMap<>();
		attachParams.put(VolumeAttachment.VOLUME_ID, params.getDiskId());
		List<VolumeAttachment> attachMents = volumeAttachmentDao.findByProperties(attachParams);
		if(null == attachMents || attachMents.isEmpty()) {
			response.setInstanceId(null);
			response.setInstanceName(null);
		} else {
			VolumeAttachment item = attachMents.get(0);
			response.setInstanceId(item.getInstanceUuid());
			if(null != item.getInstanceUuid()) {
				VmInstance vm = instanceDao.getById(item.getInstanceUuid());
				if(null != vm) {
					response.setInstanceName(vm.getAlias());
				}
			}
		}
		
		//可用区信息处�?
		String zoneId = disk.getZoneId();
		if(StringUtils.isNotBlank(zoneId)) {
			AvailableZoneEntity zone = zoneDao.getById(zoneId);
			response.setZoneName(zone == null ? null : zone.getName());
		} else {
			response.setZoneName(null);
		}
		response.setZoneId(zoneId);
		
		//创建者信息处�?
		String userName = UserUtil.userName(disk.getUserId());
		response.setCreator(userName);

		return response;
	}

	@Override
	public List<VolumeAttachmentInfo> getAttachmentVolume(String instanceId) {
		List<VolumeAttachmentInfo> result = new ArrayList<>();
		List<VolumeExInfo> exList = volumeDao.getAttachMentVolume(instanceId, VolumeExInfo.class);
		
		for (VolumeExInfo item : exList) {
			VolumeAttachmentInfo tmp = new VolumeAttachmentInfo();
			Volume volume = new Volume();
			volume.setId(item.getId());
			volume.setDisplayName(item.getDisplayName());
			volume.setStatus(item.getStatus());
			volume.setSize(item.getSize());
			volume.setDiskType(item.getDiskType());
			volume.setCreatedAt(item.getCreatedAt());
			volume.setUpdatedAt(item.getUpdatedAt());
			volume.setDescription(item.getDescription());
			volume.setSnapshotId(item.getSnapshotId());
			volume.setCategory(item.getCategory());
			volume.setBootable(item.isBootable());
			volume.setImageRef(item.getImageRef());
			volume.setUserId(item.getUserId());
			volume.setStorageType(item.getStorageType());
			volume.setPoolId(item.getPoolId());
			volume.setPoolName(item.getPoolName());
			volume.setZoneId(item.getZoneId());
			volume.setTenantId(item.getTenantId());
			volume.setProvider(item.getProvider());
			volume.setProviderRefId(item.getProviderRefId());
			tmp.setVolume(volume);
			
			VolumeAttachment attachment = new VolumeAttachment();
			attachment.setId(item.getAttachmentId());
			attachment.setInstanceUuid(item.getInstanceUuid());
			attachment.setMountpoint(item.getMountPoint());
			attachment.setVolumeId(item.getId());
			attachment.setAttachedHost(item.getAttachedHost());
			attachment.setProviderRefId(item.getAttachProvider());
			attachment.setProviderRefId(item.getProviderRefId());
			tmp.setAttachment(attachment);
			
			result.add(tmp);
		}
		return result;
	}

}