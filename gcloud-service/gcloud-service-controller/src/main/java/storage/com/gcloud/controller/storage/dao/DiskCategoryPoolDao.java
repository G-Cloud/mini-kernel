package com.gcloud.controller.storage.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.storage.entity.DiskCategoryPool;
import com.gcloud.core.util.SqlUtil;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.kenai.jffi.Array;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Repository
public class DiskCategoryPoolDao extends JdbcBaseDaoImpl<DiskCategoryPool, Integer> {
	
	public void deleteByDcpIds(List<Integer> dcpIds) {
		StringBuffer sql = new StringBuffer();
		List<Integer> values = new ArrayList<>();
		
		sql.append("delete from gc_disk_category_pools where id in (")
			.append(SqlUtil.inPreStr(dcpIds.size())).append(")");
		values.addAll(dcpIds);
		Object[] arrayValues = values.toArray();
		
		this.jdbcTemplate.update(sql.toString(), arrayValues);
	}
	
	public List<DiskCategoryPool> findByDiskCategoryIdAndZoneId(String diskCategoryId, List<String> zoneIds) {
		StringBuffer sql = new StringBuffer();
		List<Object> values = new ArrayList<>();
		
		sql.append("select * from gc_disk_category_pools where disk_category_id = ? and zone_id in (");
		sql.append(SqlUtil.inPreStr(zoneIds.size()));
		sql.append(")");
		
		values.add(diskCategoryId);
		values.addAll(zoneIds);
		
		return findBySql(sql.toString(), values);
	}
	
	public List<DiskCategoryPool> findByPoolIdAndZoneId(String poolId, List<String> zoneIds) {
		StringBuffer sql = new StringBuffer();
		List<Object> values = new ArrayList<>();
		
		sql.append("select * from gc_disk_category_pools where storage_pool_id = ? and zone_id in (");
		sql.append(SqlUtil.inPreStr(zoneIds.size()));
		sql.append(")");
		
		values.add(poolId);
		values.addAll(zoneIds);
		
		return findBySql(sql.toString(), values);
	}
	
	public List<DiskCategoryPool> findWithUniqueHost(String diskCategoryId, String poolId, String zoneId) {
		StringBuffer sql = new StringBuffer();
		List<Object> values = new ArrayList<>();
		
		sql.append("select gdc.* from gc_disk_category_pools as gdc where gdc.storage_pool_id in");
		sql.append(" (select gsp.id from gc_storage_pools as gsp where gsp.hostname = (select gsp2.hostname from gc_storage_pools as gsp2 where gsp2.id = ?))");
		sql.append("and gdc.zone_id = ? and gdc.disk_category_id = ?");
		values.add(poolId);
		values.add(zoneId);
		values.add(diskCategoryId);
		
		return findBySql(sql.toString(), values);
	}
	
	public void deleteBatchByPoolId(String diskCategoryId, String zoneId, List<String> poolIds) {
		StringBuffer sql = new StringBuffer();
		List<String> values = new ArrayList<>();
		
		sql.append("delete from gc_disk_category_pools where disk_category_id = ? and zone_id = ? and storage_pool_id in (")
			.append(SqlUtil.inPreStr(poolIds.size())).append(")");
		values.add(diskCategoryId);
		values.add(zoneId);
		values.addAll(poolIds);
		Object[] arrayValues = values.toArray();
		
		this.jdbcTemplate.update(sql.toString(), arrayValues);
	}
}