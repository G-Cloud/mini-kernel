package com.gcloud.controller.storage.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.controller.storage.entity.VolumeAttachment;
import com.gcloud.controller.utils.SqlUtil;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Repository
@Slf4j
public class VolumeAttachmentDao extends JdbcBaseDaoImpl<VolumeAttachment, String> {

	public List<VolumeAttachment> findByVolumeIdAndInstanceId(String volumeId, String instanceId) {
		String sql = "select * from gc_volume_attachments where volume_id = ? and instance_uuid = ?";

		List<Object> valueList = new ArrayList<Object>();
		valueList.add(volumeId);
		valueList.add(instanceId);
		return this.findBySql(sql, valueList);
	}

	public boolean isAttach(String volumeId){

		String sql = "select * from gc_volume_attachments where volume_id = ? limit 1";
		List<Object> values = new ArrayList<>();
		values.add(volumeId);

		List<VolumeAttachment> attachments = findBySql(sql, values);
		if(attachments != null && attachments.size() > 0){
			log.debug(String.format("==volume status== is volume attach, volumeId=%s, attachment=%s", volumeId, JSONObject.toJSONString(attachments)));
		}
		return attachments != null && attachments.size() > 0;

	}

	public List<VolumeAttachment> getInstanceRefAttach(String instanceId){

		StringBuffer sql = new StringBuffer();
		sql.append("select * from gc_volume_attachments t where t.volume_id in ");
		sql.append(" (select a.volume_id from gc_volume_attachments a where a.instance_uuid = ?)");

		List<Object> values = new ArrayList<>();
		values.add(instanceId);

		return this.findBySql(sql.toString(), values);
	}
	
	public void updateAttachHost(List<String> attachmentIds, String targetHostName) {
		if(attachmentIds == null || attachmentIds.isEmpty()) {
			return;
		}
		StringBuffer sql = new StringBuffer();
		List<Object> values = new ArrayList<>();
		sql.append("update gc_volume_attachments set ");
		sql.append(" attached_host = CASE id ");
		for (String attachId : attachmentIds) {
			sql.append(" when ? THEN ? ");
			values.add(attachId);
			values.add(targetHostName);
		}
        sql.append(" end ");
        sql.append(" where id in (").append(SqlUtil.inPreStr(attachmentIds.size())).append(")");
        values.addAll(attachmentIds);
        this.jdbcTemplate.update(sql.toString(), values.toArray());
	}
}