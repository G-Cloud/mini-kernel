package com.gcloud.controller.storage.provider.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.log.util.LongTaskUtil;
import com.gcloud.controller.storage.async.volume.GcloudCreateDiskAsyncTask;
import com.gcloud.controller.storage.dao.SnapshotDao;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.driver.IStorageDriver;
import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.entity.VolumeAttachment;
import com.gcloud.controller.storage.model.CreateDiskResponse;
import com.gcloud.controller.storage.model.CreateVolumeParams;
import com.gcloud.controller.storage.provider.IVolumeProvider;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.log.enums.LogType;
import com.gcloud.header.storage.enums.VolumeStatus;
import com.gcloud.header.storage.msg.node.volume.lvm.NodeActiveLvMsg;
import com.gcloud.header.storage.msg.node.volume.lvm.NodeUnActiveLvMsg;
import com.gcloud.service.common.lvm.uitls.LvmUtil;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
@Component
public class GcloudVolumeProvider implements IVolumeProvider {

    @Autowired
    private IVolumeService volumeService;

    @Autowired
    private VolumeDao volumeDao;

    @Autowired
    private SnapshotDao snapshotDao;
    
    @Autowired
    private MessageBus bus;
    
    @Autowired
    private GcloudCreateDiskAsyncTask gcloudCreateDiskAsyncTask;

    public static final Map<String, IStorageDriver> DRIVERS = new HashMap<>();

    @PostConstruct
    private void init() {
        for (IStorageDriver driver : SpringUtil.getBeans(IStorageDriver.class)) {
            DRIVERS.put(driver.storageType().getValue(), driver);
        }
    }

    @Override
    public ResourceType resourceType() {
        return ResourceType.VOLUME;
    }

    @Override
    public ProviderType providerType() {
        return ProviderType.GCLOUD;
    }

    public List<Volume> getVolumeList(Map<String, String> filterParams) throws GCloudException {
        return null;
    }

    public List<Snapshot> getSnapshotList(Map<String, String> filterParams) throws GCloudException {
        return null;
    }

    @Override
    public CreateDiskResponse createVolume(String volumeId, CreateVolumeParams params, CurrentUser currentUser) throws GCloudException {
    	gcloudCreateDiskAsyncTask.createVolumeAsync( volumeId,  params,  currentUser);//@Async 只有方法�?在类给spring管理才能生效
    	CreateDiskResponse response = new CreateDiskResponse();
    	response.setLogType(LogType.ASYNC);
        response.setDiskId(volumeId);
    	return response;
    }
    
    /*@Async("asyncExecutor")
    public void createVolumeAsync(String volumeId, CreateVolumeParams params, CurrentUser currentUser) throws GCloudException {
    	log.debug(String.format("createVolumeAsync %s start %s,线程ID�?%s",volumeId, new Date().getTime(), Thread.currentThread().getId()));
        Volume volume = new Volume();
        {
            volume.setId(volumeId);
            volume.setDisplayName(StringUtils.isBlank(params.getDiskName())?volumeId:params.getDiskName());
            volume.setStatus(VolumeStatus.CREATING.value());
            volume.setSize(params.getSize());
            volume.setDiskType(params.getDiskType().getValue());
            volume.setCreatedAt(new Date());
            volume.setDescription(params.getDescription());
            volume.setSnapshotId(params.getSnapshotId());
            volume.setUserId(currentUser.getId());
            volume.setTenantId(currentUser.getDefaultTenant());
            volume.setCategory(params.getDiskCategory());
            volume.setStorageType(params.getPool().getStorageType());
            volume.setPoolId(params.getPool().getId());
            volume.setPoolName(params.getPool().getPoolName());
            volume.setProvider(params.getPool().getProvider());
            volume.setProviderRefId(volume.getId());
            volume.setZoneId(params.getZoneId());
            volume.setImageRef(params.getImageProviderRefId());
        }
        this.volumeDao.save(volume);
        try{
            DRIVERS.get(volume.getStorageType()).createVolume(params.getTaskId(), params.getPool(), volume);
            LongTaskUtil.syncSucc(LogType.SYNC, params.getTaskId(), volumeId);
        }catch (Exception ex){
            //没有采用事务，直接回�?
            volumeDao.deleteById(volumeId);
            LongTaskUtil.syncFail(LogType.SYNC, params.getTaskId(), volumeId);
        }
        log.debug(String.format("createVolumeAsync %s end %s",volumeId, new Date().getTime()));
    }*/

    @Override
    public void deleteVolume(StoragePool pool, Volume volume, String taskId) throws GCloudException {
        DRIVERS.get(volume.getStorageType()).deleteVolume(taskId, pool, volume);
    }

    @Override
    public void updateVolume(String volumeRefId, String name, String description) throws GCloudException {
        // no need
    }

    @Override
    public void reserveVolume(String volumeRefId) throws GCloudException {
        // no need
    }

    @Override
    public void unreserveVolume(String volumeRefId) throws GCloudException {
        // no need
    }

    @Override
    public String attachVolume(Volume volume, String gcAttachmentId, String instanceId, String mountPoint, String hostname, String taskId) throws GCloudException {
        //lvm need to unactive lv on attachedHost
    	if(volume.getStorageType().equals(StorageType.CENTRAL.getValue())) {
    		NodeActiveLvMsg msg = new NodeActiveLvMsg();
    		msg.setLvPath(LvmUtil.getVolumePath(volume.getPoolName(), volume.getId()));
    		msg.setServiceId(MessageUtil.storageServiceId(hostname));
            msg.setTaskId(taskId);
            this.bus.call(msg);
    	}
    	return gcAttachmentId;
    }

    @Override
    public void beginDetachingVolume(Volume volume) throws GCloudException {
        // no need
    }

    @Override
    public void rollDetachingVolume(Volume volume) throws GCloudException {
        // no need
    }

    @Override
    public void detachVolume(Volume volume, VolumeAttachment attachment) throws GCloudException {
        // no need
    	//lvm need to unactive lv on attachedHost
    	if(volume.getStorageType().equals(StorageType.CENTRAL.getValue())) {
    		NodeUnActiveLvMsg msg = new NodeUnActiveLvMsg();
    		msg.setLvPath(LvmUtil.getVolumePath(volume.getPoolName(), volume.getId()));
    		msg.setServiceId(MessageUtil.storageServiceId(attachment.getAttachedHost()));
            this.bus.call(msg);
    	}
    }

    @Override
    public void resizeVolume(StoragePool pool, Volume volume, int newSize, String taskId) throws GCloudException {
        DRIVERS.get(volume.getStorageType()).resizeVolume(taskId, pool, volume, newSize);
    }

}