package com.gcloud.controller.storage.handler.api.volume;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.controller.storage.model.DescribeDisksParams;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.model.DiskItemType;
import com.gcloud.header.storage.msg.api.volume.ApiDescribeDisksMsg;
import com.gcloud.header.storage.msg.api.volume.ApiDescribeDisksReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module= Module.ECS,subModule=SubModule.DISK, action="DescribeDisks", name = "磁盘列表")
public class ApiDescribeDisksHandler extends MessageHandler<ApiDescribeDisksMsg, ApiDescribeDisksReplyMsg> {

    @Autowired
    private IVolumeService volumeService;
    
    @Autowired
    private IVmBaseService vmBaseService;

    @Override
    public ApiDescribeDisksReplyMsg handle(ApiDescribeDisksMsg msg) throws GCloudException {

        DescribeDisksParams params = BeanUtil.copyProperties(msg, DescribeDisksParams.class);
        if(StringUtils.isNotBlank(msg.getAttachInstanceId())) {
        	params.setHostname(vmBaseService.getInstanceById(msg.getAttachInstanceId()).getHostname());
        }
        PageResult<DiskItemType> response = volumeService.describeDisks(params, msg.getCurrentUser());
        ApiDescribeDisksReplyMsg replyMsg = new ApiDescribeDisksReplyMsg();
        replyMsg.init(response);
        return replyMsg;
    }
}