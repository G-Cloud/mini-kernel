package com.gcloud.controller.storage.handler.api.pool;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.model.DescribeDiskCategoriesParams;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.model.DiskCategoryType;
import com.gcloud.header.storage.msg.api.pool.ApiDescribeDiskCategoriesMsg;
import com.gcloud.header.storage.msg.api.pool.ApiDescribeDiskCategoriesReplyMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.DESCRIBE_DISK_CATEGORIES, name = "查询磁盘类型列表")
public class ApiDescribeDiskCategoriesHandler extends MessageHandler<ApiDescribeDiskCategoriesMsg, ApiDescribeDiskCategoriesReplyMsg> {

    @Autowired
    private IStoragePoolService poolService;

    @Override
    public ApiDescribeDiskCategoriesReplyMsg handle(ApiDescribeDiskCategoriesMsg msg) throws GCloudException {
    	DescribeDiskCategoriesParams params = BeanUtil.copyProperties(msg, DescribeDiskCategoriesParams.class);
        PageResult<DiskCategoryType> response = poolService.describeDiskCategories(params);
        
        ApiDescribeDiskCategoriesReplyMsg reply = new ApiDescribeDiskCategoriesReplyMsg();
        reply.init(response);
        return reply;
    }

}