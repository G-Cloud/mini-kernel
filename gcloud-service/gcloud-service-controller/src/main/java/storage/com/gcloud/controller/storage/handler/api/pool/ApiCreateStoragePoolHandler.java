package com.gcloud.controller.storage.handler.api.pool;

import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.msg.api.pool.ApiCreateStoragePoolMsg;
import com.gcloud.header.storage.msg.api.pool.ApiCreateStoragePoolReplyMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@GcLog(taskExpect="创建存储�?")
@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.CREATE_STORAGE_POOL, name = "创建存储�?")
public class ApiCreateStoragePoolHandler extends MessageHandler<ApiCreateStoragePoolMsg, ApiCreateStoragePoolReplyMsg> {

    @Autowired
    private IStoragePoolService poolService;

    @Override
    public ApiCreateStoragePoolReplyMsg handle(ApiCreateStoragePoolMsg msg) throws GCloudException {
        ApiCreateStoragePoolReplyMsg reply = new ApiCreateStoragePoolReplyMsg();
        reply.setPoolId(this.poolService.createStoragePool(msg.getDisplayName(), msg.getProvider(), msg.getStorageType(), msg.getPoolName(), msg.getZoneId(), /*msg.getCategoryId(),*/
                msg.getHostname(), msg.getDriver(), msg.getTaskId()));
        
        msg.setObjectId(reply.getPoolId());
        msg.setObjectName(msg.getDisplayName());
        return reply;
    }

}