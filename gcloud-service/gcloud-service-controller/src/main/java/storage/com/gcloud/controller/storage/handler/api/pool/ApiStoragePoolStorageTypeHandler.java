package com.gcloud.controller.storage.handler.api.pool;

import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.model.StorageTypeVo;
import com.gcloud.header.storage.msg.api.pool.ApiStoragePoolStorageTypeMsg;
import com.gcloud.header.storage.msg.api.pool.ApiStoragePoolStorageTypeReplyMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.STORAGE_POOL_STORAGE_TYPE, name = "存储池的存储类型列表")
public class ApiStoragePoolStorageTypeHandler extends MessageHandler<ApiStoragePoolStorageTypeMsg, ApiStoragePoolStorageTypeReplyMsg> {

    @Autowired
    private IStoragePoolService poolService;

    @Override
    public ApiStoragePoolStorageTypeReplyMsg handle(ApiStoragePoolStorageTypeMsg msg) throws GCloudException {
        List<StorageTypeVo> storageTypeVos = poolService.storagePoolStorageType();
        ApiStoragePoolStorageTypeReplyMsg reply = new ApiStoragePoolStorageTypeReplyMsg();
        reply.init(storageTypeVos);
        return reply;
    }
}