package com.gcloud.controller.security.model.workflow;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DeleteSecurityClusterVmInitFlowCommandRes {

    private String componentId;
    private String instanceId;
    private Boolean deleteInTask;
    private Boolean deleteNotExist;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getComponentId() {
        return componentId;
    }

    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    public Boolean getDeleteInTask() {
        return deleteInTask;
    }

    public void setDeleteInTask(Boolean deleteInTask) {
        this.deleteInTask = deleteInTask;
    }

    public Boolean getDeleteNotExist() {
        return deleteNotExist;
    }

    public void setDeleteNotExist(Boolean deleteNotExist) {
        this.deleteNotExist = deleteNotExist;
    }
}