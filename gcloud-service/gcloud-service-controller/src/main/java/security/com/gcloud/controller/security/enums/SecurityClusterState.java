package com.gcloud.controller.security.enums;

import com.google.common.base.CaseFormat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum SecurityClusterState {

    CREATING("创建�?", SecurityBaseState.RUNNING),
    CREATED("已创�?", SecurityBaseState.SUCCESS),
    DELETING("删除�?", SecurityBaseState.RUNNING),
    DELETE_FAILED("删除失败", SecurityBaseState.FAILED),
    FAILED("创建失败", SecurityBaseState.FAILED),

    ENABLING_HA("启用HA�?", SecurityBaseState.RUNNING),
    ENABLE_FAILED("启用HA失败", SecurityBaseState.FAILED),
    DISABLING_HA("取消HA�?", SecurityBaseState.RUNNING),
    DISABLE_FAILED("取消HA失败", SecurityBaseState.FAILED)
    ;

    private String cnName;
    private SecurityBaseState securityBaseState;

    SecurityClusterState(String cnName, SecurityBaseState securityBaseState) {
        this.cnName = cnName;
        this.securityBaseState = securityBaseState;
    }

    public String value() {
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
    }

    public static SecurityClusterState getByValue(String value){
        return Arrays.stream(SecurityClusterState.values()).filter(s -> s.value().equals(value)).findFirst().orElse(null);
    }

    public static Map<String, String> valueCnMap(){
        Map<String, String> result = new HashMap<>();
        Arrays.stream(SecurityClusterState.values()).forEach(s -> result.put(s.value(), s.getCnName()));
        return result;
    }

    public String getCnName() {
        return cnName;
    }

    public SecurityBaseState getSecurityBaseState() {
        return securityBaseState;
    }
}