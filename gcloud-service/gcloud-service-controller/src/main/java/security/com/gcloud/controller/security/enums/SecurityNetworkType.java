package com.gcloud.controller.security.enums;

import com.google.common.base.CaseFormat;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum SecurityNetworkType {

    PROTECTION("防护�?", true),
    MANAGEMENT("管理�?", true),
    OUTER("外网", false),
    HA("HA心跳�?", true);

    private String cnName;
    private Boolean needSecurityGroup;

    SecurityNetworkType(String cnName, Boolean needSecurityGroup) {
        this.cnName = cnName;
        this.needSecurityGroup = needSecurityGroup;
    }

    public String value() {
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
    }

    public String getName() {
        return cnName;
    }

    public Boolean getNeedSecurityGroup() {
        return needSecurityGroup;
    }

    public static SecurityNetworkType getByValue(String value){
        return Arrays.stream(SecurityNetworkType.values()).filter(t -> t.value().equals(value)).findFirst().orElse(null);
    }
}